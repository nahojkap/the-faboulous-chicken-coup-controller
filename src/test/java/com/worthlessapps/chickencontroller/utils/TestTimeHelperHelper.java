package com.worthlessapps.chickencontroller.utils;

import org.junit.Assert;
import org.junit.Test;

import java.util.regex.Matcher;

/**
 * @author
 */
public class TestTimeHelperHelper {

    @Test
    public void sunriseParserPositiveOffset() {

        Matcher sunriseSunsetMatcher = TimeHelper.createSunriseSunsetMatcher("sunrise+1m");
        Assert.assertTrue(sunriseSunsetMatcher.matches());
        Assert.assertEquals(4, sunriseSunsetMatcher.groupCount());

        sunriseSunsetMatcher = TimeHelper.createSunriseSunsetMatcher("sunrise+1");
        Assert.assertTrue(sunriseSunsetMatcher.matches());
        Assert.assertEquals(4, sunriseSunsetMatcher.groupCount());

        sunriseSunsetMatcher = TimeHelper.createSunriseSunsetMatcher("sunrise+99");
        Assert.assertTrue(sunriseSunsetMatcher.matches());
        Assert.assertEquals(4, sunriseSunsetMatcher.groupCount());

        sunriseSunsetMatcher = TimeHelper.createSunriseSunsetMatcher("sunrise+99m");
        Assert.assertTrue(sunriseSunsetMatcher.matches());
        Assert.assertEquals(4, sunriseSunsetMatcher.groupCount());
    }

    @Test
    public void sunriseParserNegativeOffset() {

        Matcher sunriseSunsetMatcher = TimeHelper.createSunriseSunsetMatcher("sunrise-1m");
        Assert.assertTrue(sunriseSunsetMatcher.matches());
        Assert.assertEquals(4, sunriseSunsetMatcher.groupCount());

        sunriseSunsetMatcher = TimeHelper.createSunriseSunsetMatcher("sunrise-1");
        Assert.assertTrue(sunriseSunsetMatcher.matches());
        Assert.assertEquals(4, sunriseSunsetMatcher.groupCount());

        sunriseSunsetMatcher = TimeHelper.createSunriseSunsetMatcher("sunrise-99");
        Assert.assertTrue(sunriseSunsetMatcher.matches());
        Assert.assertEquals(4, sunriseSunsetMatcher.groupCount());

        sunriseSunsetMatcher = TimeHelper.createSunriseSunsetMatcher("sunrise-99m");
        Assert.assertTrue(sunriseSunsetMatcher.matches());
        Assert.assertEquals(4, sunriseSunsetMatcher.groupCount());
    }

    @Test
    public void sunriseParserBadOffset() {
        Assert.assertFalse(TimeHelper.createSunriseSunsetMatcher("sunrise+h").matches());
    }

    @Test
    public void sunriseParserTooLargeOffset() {
        Assert.assertFalse(TimeHelper.createSunriseSunsetMatcher("sunrise+100m").matches());
    }


    @Test
    public void durationParser() {

        Matcher durationMatcher = TimeHelper.createDurationMatcher("10");
        Assert.assertTrue(durationMatcher.matches());
        Assert.assertEquals(2, durationMatcher.groupCount());

        durationMatcher = TimeHelper.createDurationMatcher("10m");
        Assert.assertTrue(durationMatcher.matches());
        Assert.assertEquals(2, durationMatcher.groupCount());

    }

    @Test
    public void durationParserUnits() {

        Matcher durationMatcher = TimeHelper.createDurationMatcher("10m");
        Assert.assertTrue(durationMatcher.matches());
        Assert.assertEquals(2, durationMatcher.groupCount());

        durationMatcher = TimeHelper.createDurationMatcher("10h");
        Assert.assertTrue(durationMatcher.matches());
        Assert.assertEquals(2, durationMatcher.groupCount());

        durationMatcher = TimeHelper.createDurationMatcher("10d");
        Assert.assertTrue(durationMatcher.matches());
        Assert.assertEquals(2, durationMatcher.groupCount());

    }

    @Test
    public void durationParserTooLargeValue() {
        Assert.assertFalse(TimeHelper.createDurationMatcher("1000m").matches());
    }

    @Test
    public void durationParserNoNumber() {
        Assert.assertFalse(TimeHelper.createDurationMatcher("m").matches());
    }

    @Test
    public void simpleTimeParser()
    {
        Matcher simpleTimeParser = TimeHelper.createTimeMatcher("7:00");
        Assert.assertTrue(simpleTimeParser.matches());
        Assert.assertEquals(7, simpleTimeParser.groupCount());
        Assert.assertEquals("7:00",simpleTimeParser.group(1));
        Assert.assertEquals("7",simpleTimeParser.group(2));
        Assert.assertEquals("00",simpleTimeParser.group(3));
        Assert.assertNull(simpleTimeParser.group(4));
        Assert.assertNull(simpleTimeParser.group(5));
        Assert.assertNull(simpleTimeParser.group(6));

        simpleTimeParser = TimeHelper.createTimeMatcher("07:00");
        Assert.assertTrue(simpleTimeParser.matches());
        Assert.assertEquals(7, simpleTimeParser.groupCount());
        Assert.assertEquals("07:00",simpleTimeParser.group(1));
        Assert.assertEquals("07",simpleTimeParser.group(2));
        Assert.assertEquals("00",simpleTimeParser.group(3));
        Assert.assertNull(simpleTimeParser.group(4));
        Assert.assertNull(simpleTimeParser.group(5));
        Assert.assertNull(simpleTimeParser.group(6));

        simpleTimeParser = TimeHelper.createTimeMatcher("23:59");
        Assert.assertTrue(simpleTimeParser.matches());
        Assert.assertEquals(7, simpleTimeParser.groupCount());
        Assert.assertEquals("23:59",simpleTimeParser.group(1));
        Assert.assertEquals("23",simpleTimeParser.group(2));
        Assert.assertEquals("59",simpleTimeParser.group(3));
        Assert.assertNull(simpleTimeParser.group(4));
        Assert.assertNull(simpleTimeParser.group(5));
        Assert.assertNull(simpleTimeParser.group(6));

        simpleTimeParser = TimeHelper.createTimeMatcher("00:00");
        Assert.assertTrue(simpleTimeParser.matches());
        Assert.assertEquals(7, simpleTimeParser.groupCount());
        Assert.assertEquals("00:00",simpleTimeParser.group(1));
        Assert.assertEquals("00",simpleTimeParser.group(2));
        Assert.assertEquals("00",simpleTimeParser.group(3));
        Assert.assertNull(simpleTimeParser.group(4));
        Assert.assertNull(simpleTimeParser.group(5));
        Assert.assertNull(simpleTimeParser.group(6));

        simpleTimeParser = TimeHelper.createTimeMatcher("7:00am");
        Assert.assertTrue(simpleTimeParser.matches());
        Assert.assertEquals(7, simpleTimeParser.groupCount());
        Assert.assertNull(simpleTimeParser.group(1));
        Assert.assertNull(simpleTimeParser.group(2));
        Assert.assertNull(simpleTimeParser.group(3));
        Assert.assertEquals("7:00am",simpleTimeParser.group(4));
        Assert.assertEquals("7",simpleTimeParser.group(5));
        Assert.assertEquals("00",simpleTimeParser.group(6));
        Assert.assertEquals("am",simpleTimeParser.group(7));

        simpleTimeParser = TimeHelper.createTimeMatcher("11:59pm");
        Assert.assertTrue(simpleTimeParser.matches());
        Assert.assertEquals(7, simpleTimeParser.groupCount());
        Assert.assertNull(simpleTimeParser.group(1));
        Assert.assertNull(simpleTimeParser.group(2));
        Assert.assertNull(simpleTimeParser.group(3));
        Assert.assertEquals("11:59pm",simpleTimeParser.group(4));
        Assert.assertEquals("11",simpleTimeParser.group(5));
        Assert.assertEquals("59",simpleTimeParser.group(6));
        Assert.assertEquals("pm",simpleTimeParser.group(7));

        simpleTimeParser = TimeHelper.createTimeMatcher("0:00am");
        Assert.assertTrue(simpleTimeParser.matches());
        Assert.assertEquals(7, simpleTimeParser.groupCount());
        Assert.assertNull(simpleTimeParser.group(1));
        Assert.assertNull(simpleTimeParser.group(2));
        Assert.assertNull(simpleTimeParser.group(3));
        Assert.assertEquals("0:00am",simpleTimeParser.group(4));
        Assert.assertEquals("0",simpleTimeParser.group(5));
        Assert.assertEquals("00",simpleTimeParser.group(6));
        Assert.assertEquals("am",simpleTimeParser.group(7));

        simpleTimeParser = TimeHelper.createTimeMatcher("0:00 AM");
        Assert.assertTrue(simpleTimeParser.matches());
        Assert.assertEquals(7, simpleTimeParser.groupCount());
        Assert.assertNull(simpleTimeParser.group(1));
        Assert.assertNull(simpleTimeParser.group(2));
        Assert.assertNull(simpleTimeParser.group(3));
        Assert.assertEquals("0:00 AM",simpleTimeParser.group(4));
        Assert.assertEquals("0",simpleTimeParser.group(5));
        Assert.assertEquals("00",simpleTimeParser.group(6));
        Assert.assertEquals("AM",simpleTimeParser.group(7));

        simpleTimeParser = TimeHelper.createTimeMatcher("3:00 pM");
        Assert.assertTrue(simpleTimeParser.matches());
        Assert.assertEquals(7, simpleTimeParser.groupCount());
        Assert.assertNull(simpleTimeParser.group(1));
        Assert.assertNull(simpleTimeParser.group(2));
        Assert.assertNull(simpleTimeParser.group(3));
        Assert.assertEquals("3:00 pM",simpleTimeParser.group(4));
        Assert.assertEquals("3",simpleTimeParser.group(5));
        Assert.assertEquals("00",simpleTimeParser.group(6));
        Assert.assertEquals("pM",simpleTimeParser.group(7));


        simpleTimeParser = TimeHelper.createTimeMatcher("9:59 pM");
        Assert.assertTrue(simpleTimeParser.matches());
        Assert.assertEquals(7, simpleTimeParser.groupCount());
        Assert.assertNull(simpleTimeParser.group(1));
        Assert.assertNull(simpleTimeParser.group(2));
        Assert.assertNull(simpleTimeParser.group(3));
        Assert.assertEquals("9:59 pM",simpleTimeParser.group(4));
        Assert.assertEquals("9",simpleTimeParser.group(5));
        Assert.assertEquals("59",simpleTimeParser.group(6));
        Assert.assertEquals("pM",simpleTimeParser.group(7));

        simpleTimeParser = TimeHelper.createTimeMatcher("12:59 pM");
        Assert.assertTrue(simpleTimeParser.matches());
        Assert.assertEquals(7, simpleTimeParser.groupCount());
        Assert.assertNull(simpleTimeParser.group(1));
        Assert.assertNull(simpleTimeParser.group(2));
        Assert.assertNull(simpleTimeParser.group(3));
        Assert.assertEquals("12:59 pM",simpleTimeParser.group(4));
        Assert.assertEquals("12",simpleTimeParser.group(5));
        Assert.assertEquals("59",simpleTimeParser.group(6));
        Assert.assertEquals("pM",simpleTimeParser.group(7));

        simpleTimeParser = TimeHelper.createTimeMatcher("12:59am");
        Assert.assertTrue(simpleTimeParser.matches());
        Assert.assertEquals(7, simpleTimeParser.groupCount());
        Assert.assertNull(simpleTimeParser.group(1));
        Assert.assertNull(simpleTimeParser.group(2));
        Assert.assertNull(simpleTimeParser.group(3));
        Assert.assertEquals("12:59am",simpleTimeParser.group(4));
        Assert.assertEquals("12",simpleTimeParser.group(5));
        Assert.assertEquals("59",simpleTimeParser.group(6));
        Assert.assertEquals("am",simpleTimeParser.group(7));

    }


    @Test
    public void simpleTimeParserMixing12and24HourTIme()
    {
        Matcher simpleTimeParser = TimeHelper.createTimeMatcher("23:00pm");
        Assert.assertFalse(simpleTimeParser.matches());

        simpleTimeParser = TimeHelper.createTimeMatcher("01:00am");
        Assert.assertFalse(simpleTimeParser.matches());

        simpleTimeParser = TimeHelper.createTimeMatcher("00:00 fsdfds");
        Assert.assertFalse(simpleTimeParser.matches());

        simpleTimeParser = TimeHelper.createTimeMatcher("-1:00am");
        Assert.assertFalse(simpleTimeParser.matches());

    }

    @Test
    public void simpleTimeParserOutOfRange()
    {
        Matcher simpleTimeParser = TimeHelper.createTimeMatcher("24:00");
        Assert.assertFalse(simpleTimeParser.matches());

        simpleTimeParser = TimeHelper.createTimeMatcher("23:60");
        Assert.assertFalse(simpleTimeParser.matches());

        simpleTimeParser = TimeHelper.createTimeMatcher("-0:01");
        Assert.assertFalse(simpleTimeParser.matches());

        simpleTimeParser = TimeHelper.createTimeMatcher("13:00am");
        Assert.assertFalse(simpleTimeParser.matches());

        simpleTimeParser = TimeHelper.createTimeMatcher("12:60am");
        Assert.assertFalse(simpleTimeParser.matches());

        simpleTimeParser = TimeHelper.createTimeMatcher("12:60am");
        Assert.assertFalse(simpleTimeParser.matches());

        simpleTimeParser = TimeHelper.createTimeMatcher("07:00am");
        Assert.assertFalse(simpleTimeParser.matches());
    }

    @Test
    public void addStringifiedTime()
    {

        Assert.assertEquals(1599451200000L, TimeHelper.addStringifiedTime(1599523200000L,"00:00"));
        Assert.assertEquals(1599476400000L, TimeHelper.addStringifiedTime(1599523200000L,"7:00"));
        Assert.assertEquals(1599519600000L, TimeHelper.addStringifiedTime(1599523200000L,"19:00"));
        Assert.assertEquals(1599537540000L, TimeHelper.addStringifiedTime(1599523200000L,"23:59"));

        Assert.assertEquals(1599451200000L, TimeHelper.addStringifiedTime(1599523200000L,"0:00am"));
        Assert.assertEquals(1599476400000L, TimeHelper.addStringifiedTime(1599523200000L,"7:00am"));
        Assert.assertEquals(1599519600000L, TimeHelper.addStringifiedTime(1599523200000L,"7:00pm"));
        Assert.assertEquals(1599537540000L, TimeHelper.addStringifiedTime(1599523200000L,"11:59pm"));

    }

}
