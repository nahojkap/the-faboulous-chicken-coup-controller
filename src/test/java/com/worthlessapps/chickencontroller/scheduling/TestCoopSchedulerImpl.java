package com.worthlessapps.chickencontroller.scheduling;

import com.coreoz.wisp.schedule.Schedule;
import com.worthlessapps.chickencontroller.CoopDoor;
import com.worthlessapps.chickencontroller.location.LocationResolver;
import com.worthlessapps.chickencontroller.utils.TimeHelper;
import org.joda.time.format.ISODateTimeFormat;
import org.junit.Assert;
import org.junit.Test;

import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static com.worthlessapps.chickencontroller.testutils.ChickenCoopConfigurationTestHelper.getMockedChickenCoupConfiguration;
import static com.worthlessapps.chickencontroller.testutils.ChickenCoopConfigurationTestHelper.getMockedChickenCoupConfigurationSunriseBasedSchedule;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

/**
 * @author
 */
public class TestCoopSchedulerImpl {


    @Test
    public void createScheduleFromEntrySunrise() {
        CoopSchedulerImpl coopScheduler = new CoopSchedulerImpl(getMockedChickenCoupConfiguration(), null, null, null, new LocationResolver() {
            @Override
            public Future<Location> resolveLocation() {
                return Executors.newSingleThreadExecutor().submit(()->{ return new Location(1,1);});
            }
        });
        final Schedule scheduleFromEntry = coopScheduler.createScheduleFromEntry("sunrise+1m");

        final long nextExecutionInMillis = scheduleFromEntry.nextExecutionInMillis(1600798102162L, 0, null);
        Assert.assertEquals(1600839959000L, nextExecutionInMillis);
                                     

    }

    @Test
    public void createScheduleFromEntrySpecificTime() {
        CoopSchedulerImpl coopScheduler = new CoopSchedulerImpl(getMockedChickenCoupConfiguration(), null, null, null, new LocationResolver() {
            @Override
            public Future<Location> resolveLocation() {
                return Executors.newSingleThreadExecutor().submit(()->{ return new Location(1,1);});
            }
        });

        System.out.println(ISODateTimeFormat.dateTime().print(1600798102162L));

        Schedule scheduleFromEntry = coopScheduler.createScheduleFromEntry("07:00");
        long nextExecutionInMillis = scheduleFromEntry.nextExecutionInMillis(1600798102162L, 0, null);
        Assert.assertEquals(1600858800000L, nextExecutionInMillis);
        nextExecutionInMillis = scheduleFromEntry.nextExecutionInMillis(1600772400000L, 1, null);
        Assert.assertEquals(1600945200000L, nextExecutionInMillis);

        scheduleFromEntry = coopScheduler.createScheduleFromEntry("7:00am");
        nextExecutionInMillis = scheduleFromEntry.nextExecutionInMillis(1600798102162L, 0, null);
        Assert.assertEquals(1600858800000L, nextExecutionInMillis);
        nextExecutionInMillis = scheduleFromEntry.nextExecutionInMillis(1600772400000L, 1, null);
        Assert.assertEquals(1600945200000L, nextExecutionInMillis);

        scheduleFromEntry = coopScheduler.createScheduleFromEntry("19:00");
        nextExecutionInMillis = scheduleFromEntry.nextExecutionInMillis(1600798102162L, 0, null);
        Assert.assertEquals(1600815600000L, nextExecutionInMillis);
        nextExecutionInMillis = scheduleFromEntry.nextExecutionInMillis(1600815600000L, 1, null);
        Assert.assertEquals(1600902000000L, nextExecutionInMillis);

        scheduleFromEntry = coopScheduler.createScheduleFromEntry("7:00pm");
        nextExecutionInMillis = scheduleFromEntry.nextExecutionInMillis(1600798102162L, 0, null);
        Assert.assertEquals(1600815600000L, nextExecutionInMillis);
        nextExecutionInMillis = scheduleFromEntry.nextExecutionInMillis(1600815600000L, 1, null);
        Assert.assertEquals(1600902000000L, nextExecutionInMillis);

    }

    @Test
    public void shouldBeClosedSimpleTimebased()
    {
        CoopDoor coopDoorMock = mock(CoopDoor.class);

        CoopSchedulerImpl coopScheduler = new CoopSchedulerImpl(getMockedChickenCoupConfiguration(), coopDoorMock, null, null, new LocationResolver() {
            @Override
            public Future<Location> resolveLocation() {
                return Executors.newSingleThreadExecutor().submit(()->{ return new Location(1,1);});
            }
        });

        coopScheduler.initialize();

        assertTrue(coopScheduler.shouldBeClosed(TimeHelper.getMidnight(1600798102162L)));
        assertFalse(coopScheduler.shouldBeClosed(TimeHelper.getMidnight(1600798102162L).minusHours(6)));
        assertFalse(coopScheduler.shouldBeClosed(TimeHelper.getMidnight(1600798102162L).minusHours(5)));
        assertTrue(coopScheduler.shouldBeClosed(TimeHelper.getMidnight(1600798102162L).minusHours(4).minusMinutes(59)));
        assertTrue(coopScheduler.shouldBeClosed(TimeHelper.getMidnight(1600798102162L).plusHours(6).plusMinutes(59)));
        assertFalse(coopScheduler.shouldBeClosed(TimeHelper.getMidnight(1600798102162L).plusHours(7)));

        coopScheduler.shutdown();

        // verifyNoMoreInteractions(coopDoorMock);
    }


    @Test
    public void shouldBeClosedSunsetBased()
    {
        CoopDoor coopDoorMock = mock(CoopDoor.class);
        CoopSchedulerImpl coopScheduler = new CoopSchedulerImpl(getMockedChickenCoupConfigurationSunriseBasedSchedule(), coopDoorMock, null, null, new LocationResolver() {
            @Override
            public Future<Location> resolveLocation() {
                return Executors.newSingleThreadExecutor().submit(()->{ return new Location(28.435710906982422, -80.6852798461914);});
            }
        });

        coopDoorMock.close(true);

        coopScheduler.initialize();

        // Closing time would be 2020-09-21T19:04:51.000-04:00
        // Opening time would be 2020-09-22T07:26:28.000-04:00

        assertFalse(coopScheduler.shouldBeClosed(TimeHelper.getMidnight(1600798102162L).minusHours(6)));
        assertFalse(coopScheduler.shouldBeClosed(TimeHelper.getMidnight(1600798102162L).minusHours(5)));
        assertTrue(coopScheduler.shouldBeClosed(TimeHelper.getMidnight(1600798102162L).minusHours(4).minusMinutes(55).minusSeconds(9)));
        assertTrue(coopScheduler.shouldBeClosed(TimeHelper.getMidnight(1600798102162L)));
        assertTrue(coopScheduler.shouldBeClosed(TimeHelper.getMidnight(1600798102162L).plusHours(6).plusMinutes(59)));
        assertFalse(coopScheduler.shouldBeClosed(TimeHelper.getMidnight(1600798102162L).plusHours(7).plusMinutes(26).plusSeconds(29)));

        coopScheduler.shutdown();
        // verifyNoMoreInteractions(coopDoorMock);

    }

    @Test
    public void isSuspended()
    {
        CoopDoor coopDoorMock = mock(CoopDoor.class);
        CoopSchedulerImpl coopScheduler = new CoopSchedulerImpl(getMockedChickenCoupConfigurationSunriseBasedSchedule(), coopDoorMock, null, null, new LocationResolver() {
            @Override
            public Future<Location> resolveLocation() {
                return Executors.newSingleThreadExecutor().submit(()->{ return new Location(28.435710906982422, -80.6852798461914);});
            }
        });

        coopScheduler.suspend();
        assertTrue(coopScheduler.isSuspended());
        assertTrue(coopScheduler.isSuspended(CoopScheduler.CoopSchedulerAction.OPEN));


    }

    @Test
    public void isSuspendedSpecificActions()
    {
        CoopDoor coopDoorMock = mock(CoopDoor.class);
        CoopSchedulerImpl coopScheduler = new CoopSchedulerImpl(getMockedChickenCoupConfigurationSunriseBasedSchedule(), coopDoorMock, null, null, new LocationResolver() {
            @Override
            public Future<Location> resolveLocation() {
                return Executors.newSingleThreadExecutor().submit(()->{ return new Location(28.435710906982422, -80.6852798461914);});
            }
        });

        coopScheduler.suspend(CoopScheduler.CoopSchedulerAction.CLOSE);
        assertFalse(coopScheduler.isSuspended());
        assertTrue(coopScheduler.isSuspended(CoopScheduler.CoopSchedulerAction.CLOSE));
        assertFalse(coopScheduler.isSuspended(CoopScheduler.CoopSchedulerAction.OPEN));


    }


}
