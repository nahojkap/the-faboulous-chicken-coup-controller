package com.worthlessapps.chickencontroller.testutils;

import com.google.gson.Gson;
import com.worthlessapps.chickencontroller.config.ChickenCoopConfig;

import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * @author
 */
public class ChickenCoopConfigurationTestHelper {

    public static ChickenCoopConfig getMockedChickenCoupConfiguration()
    {
        Gson gson = new Gson();
        final InputStream resourceAsStream = ChickenCoopConfig.class.getClassLoader().getResourceAsStream("mocked-config.json");
        return gson.fromJson(new InputStreamReader(resourceAsStream), ChickenCoopConfig.class);
    }

    public static ChickenCoopConfig getMockedChickenCoupConfigurationSunriseBasedSchedule()
    {
        Gson gson = new Gson();
        final InputStream resourceAsStream = ChickenCoopConfig.class.getClassLoader().getResourceAsStream("mocked-config-sunrise-based-schedule.json");
        return gson.fromJson(new InputStreamReader(resourceAsStream), ChickenCoopConfig.class);
    }


}
