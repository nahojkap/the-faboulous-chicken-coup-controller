package com.worthlessapps.chickencontroller;

import com.pi4j.io.gpio.*;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;
import com.worthlessapps.chickencontroller.comms.CoopDoorStatusMessage;
import com.worthlessapps.chickencontroller.config.ChickenCoopConfig;
import com.worthlessapps.chickencontroller.events.EventBuilder;
import com.worthlessapps.chickencontroller.events.EventManager;
import com.worthlessapps.chickencontroller.notifications.NotificationManager;
import com.worthlessapps.chickencontroller.utils.Logger;
import com.worthlessapps.chickencontroller.utils.Utils;

import java.util.*;

import static com.worthlessapps.chickencontroller.utils.GPIOUtils.resolvePin;
import static com.worthlessapps.chickencontroller.comms.CoopDoorStatusMessage.PinState.HIGH;
import static com.worthlessapps.chickencontroller.comms.CoopDoorStatusMessage.PinState.LOW;

public class CoopDoorImpl implements CoopDoor
{

    private CoupDoorState _coupDoorState = CoupDoorState.UNKNOWN;
    private Timer _timer;
    private GpioPinDigitalInput _hardwareSwitchDigitalInput;
    private GpioPinDigitalInput _openDigitalInput;
    private GpioPinDigitalInput _closedDigitalInput;
    private GpioPinPwmOutput _motorPwnOutput;
    private GpioPinDigitalOutput _motorDirectionDigitalOutput;
    private long _startLastOperationMs;

    private boolean _lastCloseOperationSuccessful;
    private long _lastCloseOperationDurationMs = 0;
    private boolean _lastOpenOperationSuccessful;
    private long _lastOpenOperationDurationMs = 0;

    private TimerTask _unconditionalShutdownTimerTask;

    ChickenCoopConfig.HardwareConfiguration.DoorConfiguration _doorConfiguration;

    private NotificationManager _notificationManager;
    private final String TAG = "CoupDoor";
    private EventManager _eventManager;

    private int _maxNumCoupDoorStateTransitions = 10;
    private List<CoopDoorStatusMessage.CoupDoorStateTransition> _coupDoorStateTransitions = new LinkedList<>();

    public CoopDoorImpl(NotificationManager notificationManager, EventManager eventManager) {
        _notificationManager = notificationManager;
        _eventManager = eventManager;
    }

    private synchronized void gotoCoupDoorState(CoupDoorState newCoupDoorState)
    {
        CoupDoorState oldCoupDoorState = _coupDoorState;
        
        if (_coupDoorState == newCoupDoorState)
        {
            Logger.debug("State transition into same state, will ignore");
            return;
        }

        long timestamp = System.currentTimeMillis();
        long diffFromLast = 0;
        if (_coupDoorStateTransitions.size() > 0)
        {
            diffFromLast = timestamp - _coupDoorStateTransitions.get(0)._timestampMs;
        }
        _coupDoorStateTransitions.add(0,new CoopDoorStatusMessage.CoupDoorStateTransition(_coupDoorState, newCoupDoorState, timestamp, diffFromLast));
        if (_coupDoorStateTransitions.size() > _maxNumCoupDoorStateTransitions)
        {
            _coupDoorStateTransitions.remove(_coupDoorStateTransitions.size()-1);
        }
        _coupDoorState = newCoupDoorState;

        switch (_coupDoorState)
        {
            case CLOSED:
                signalError(false);
                _eventManager.sendEvent(EventBuilder.create().category(CoopDoorStatusMessage.EventCategory.NOTIFICATION).message("Door closed").createEvent());
                break;
            case OPEN:
                signalError(false);
                _eventManager.sendEvent(EventBuilder.create().category(CoopDoorStatusMessage.EventCategory.NOTIFICATION).message("Door opened").createEvent());
                break;
            case ERROR:
                signalError(true);
                break;
            default:
                break;
        }

    }

    private void signalError(boolean inError)
    {
        if (inError)
        {
            _notificationManager.setLEDState(NotificationManager.LEDType.RED, false);
            _notificationManager.blinkLED(NotificationManager.LEDType.RED, -1, 200, 400);
        }
        else
        {
            _notificationManager.stopBlinkLED(NotificationManager.LEDType.RED);
            _notificationManager.setLEDState(NotificationManager.LEDType.RED, true);
        }
    }

    public void initialize(ChickenCoopConfig.HardwareConfiguration.DoorConfiguration doorConfiguration) {
        _doorConfiguration = doorConfiguration;

        _timer = new Timer();

        final GpioController gpio = GpioFactory.getInstance();
        _openDigitalInput = gpio.provisionDigitalInputPin(resolvePin(_doorConfiguration._openSwitchGPIO), PinPullResistance.PULL_DOWN);
        _closedDigitalInput = gpio.provisionDigitalInputPin(resolvePin(_doorConfiguration._closedSwitchGPIO), PinPullResistance.PULL_DOWN);
        _hardwareSwitchDigitalInput = gpio.provisionDigitalInputPin(resolvePin(_doorConfiguration._hardwareSwitchGPIO), PinPullResistance.PULL_DOWN);

        _motorPwnOutput = gpio.provisionPwmOutputPin(resolvePin(_doorConfiguration._motorPwnGPIO),0);
        _motorDirectionDigitalOutput = gpio.provisionDigitalOutputPin(resolvePin(_doorConfiguration._motorDirectionGPIO), PinState.LOW);

        updateCoupDoorState();

        // Add listeners to both open/closed GPIO for state changes
        _openDigitalInput.addListener(new GpioPinListenerDigital() {
            @Override
            public synchronized void handleGpioPinDigitalStateChangeEvent(GpioPinDigitalStateChangeEvent event) {

                if (event.getState() == PinState.HIGH && _coupDoorState == CoupDoorState.OPENING) {

                    _unconditionalShutdownTimerTask.cancel();

                    // Goto state OPEN
                    gotoCoupDoorState(CoupDoorState.OPEN);

                    Logger.debug("Door reached open state");

                    Utils.exceptionLessSleep(_doorConfiguration._runMotorExtraMs);

                    // Turn off the motor
                    _motorPwnOutput.setPwm(0);

                    _notificationManager.setLEDState(NotificationManager.LEDType.GREEN, false);

                    _lastOpenOperationDurationMs = System.currentTimeMillis() - _startLastOperationMs;

                    Logger.debug("Coup door is successfully opened after " + _lastOpenOperationDurationMs + " millisecond(s)");

                }
                else if (_coupDoorState == CoupDoorState.CLOSING && event.getState() == PinState.LOW)
                {
                    // This means we are leaving open mode, supposedly a good indication the door is moving in the
                    // right direction
                    Logger.debug("Door leaving open state as expected, good");
                }

            }
        });
        _closedDigitalInput.addListener(new GpioPinListenerDigital() {
            @Override
            public synchronized void handleGpioPinDigitalStateChangeEvent(GpioPinDigitalStateChangeEvent event) {

                if (event.getState() == PinState.HIGH && _coupDoorState == CoupDoorState.CLOSING) {

                    _unconditionalShutdownTimerTask.cancel();

                    Logger.debug("Door reached closed state");

                    // Goto state CLOSED
                    gotoCoupDoorState(CoupDoorState.CLOSED);

                    Utils.exceptionLessSleep(_doorConfiguration._runMotorExtraMs);

                    // Turn of the motor
                    _motorPwnOutput.setPwm(0);

                    _notificationManager.setLEDState(NotificationManager.LEDType.GREEN, false);

                    _lastCloseOperationDurationMs = System.currentTimeMillis() - _startLastOperationMs;
                    Logger.debug("Coup door is successfully closed after " + _lastCloseOperationDurationMs + " millisecond(s)");
                }
                else if (_coupDoorState == CoupDoorState.OPENING && event.getState() == PinState.LOW)
                {
                    // This means we are leaving closed mode, supposedly a good indication the door is moving in the
                    // right direction
                    Logger.debug("Door leaving closed state as expected, good");
                }
            }
        });

        _hardwareSwitchDigitalInput.addListener(new GpioPinListenerDigital() {
            @Override
            public synchronized void handleGpioPinDigitalStateChangeEvent(GpioPinDigitalStateChangeEvent event) {

                if (event.getState() == PinState.HIGH) {

                    Logger.info("Hardware switch initiated");

                    CoupDoorState coupDoorState = getCoupDoorState();
                    if (coupDoorState == CoupDoorState.OPEN) {
                        Logger.debug("Coup door is open, will initiate close");
                        close(false);
                    } else if (coupDoorState == CoupDoorState.CLOSED) {
                        Logger.debug("Coup door is closed, will initiate open");
                        open(false);
                    }
                }

            }
        });

    }

    @Override
    public void open(boolean unconditional) {

        // Ensure we are in CLOSED state
        if (!unconditional && getCoupDoorState() != CoupDoorState.CLOSED)
        {
            Logger.warn("Coup door is not in closed state, will ignore command");
            return;
        }

        int pwmValue =_motorPwnOutput.getPwm();
        if (pwmValue != 0)
        {
            Logger.warn("Coup door is already moving, will ignore command");
            // Motor is already running, something is not right!
            return;
        }

        Logger.debug("Coup door opening initiating");

        _notificationManager.setLEDState(NotificationManager.LEDType.GREEN, true);
        gotoCoupDoorState(CoupDoorState.OPENING);

        // Add timer for X seconds to ensure motor does not run forever by mistake
        scheduleUnconditionalShutdownOfMotor(_doorConfiguration._maximumMotorRuntimeSec * 1000);

        _startLastOperationMs = System.currentTimeMillis();
        // Set motor direction GPIO to "UP"
        // Set motor Pwn GPIO to "GO"
        setupMotorDirection(MotorDirection.UP);

        _motorPwnOutput.setPwm(1024/(100/_doorConfiguration._motorSpeedPercentage));

        Logger.debug("Coup door opening initiated");

    }

    enum MotorDirection { UP, DOWN }

    private void setupMotorDirection(MotorDirection motorDirection) {
        switch (motorDirection)
        {
            case UP: {
                if (_doorConfiguration._motorDirectionGPIOHighIsUp)
                    _motorDirectionDigitalOutput.high();
                else
                    _motorDirectionDigitalOutput.low();

            }
                break;
            case DOWN: {
                if (_doorConfiguration._motorDirectionGPIOHighIsUp)
                    _motorDirectionDigitalOutput.low();
                else
                    _motorDirectionDigitalOutput.high();
            }
                break;
            default:
                throw new IllegalArgumentException("Unhandled motor direction: " + motorDirection);
        }
    }

    @Override
    public void down(int durationMs) {

        scheduleUnconditionalShutdownOfMotor(durationMs);

        _startLastOperationMs = System.currentTimeMillis();
        gotoCoupDoorState(CoupDoorState.CLOSING);
        // Set motor direction GPIO to "UP"
        // Set motor Pwn GPIO to "GO"
        setupMotorDirection(MotorDirection.DOWN);
        _motorPwnOutput.setPwm(1024/(100/_doorConfiguration._motorSpeedPercentage));


    }

    @Override
    public void up(int durationMs) {

        scheduleUnconditionalShutdownOfMotor(durationMs);

        _startLastOperationMs = System.currentTimeMillis();
        gotoCoupDoorState(CoupDoorState.OPENING);
        // Set motor direction GPIO to "DOWN"
        // Set motor Pwn GPIO to "GO"
        setupMotorDirection(MotorDirection.UP);
        _motorPwnOutput.setPwm(1024/(100/_doorConfiguration._motorSpeedPercentage));

    }

    private void scheduleUnconditionalShutdownOfMotor(int shutdownAfterMs) {

        Logger.debug("Scheduling unconditional shutdown after " + shutdownAfterMs + " millisecond(s)");

        final long scheduledAtMs = System.currentTimeMillis();

        _unconditionalShutdownTimerTask = new TimerTask() {
            @Override
            public void run() {
                CoupDoorState coupDoorState = getCoupDoorState();
                if (coupDoorState == CoupDoorState.CLOSING || coupDoorState == CoupDoorState.OPENING)
                {
                    if (_motorPwnOutput.getPwm() != 0) {
                        Logger.debug("Unconditionally turning off motor after " + (System.currentTimeMillis() - scheduledAtMs));
                        // Unconditionally turn off the motor!
                        _notificationManager.setLEDState(NotificationManager.LEDType.GREEN, false);
                        _motorPwnOutput.setPwm(0);
                        final CoupDoorState newCoupDoorState = updateCoupDoorState();

                        gotoCoupDoorState(newCoupDoorState == CoupDoorState.CLOSED || newCoupDoorState == CoupDoorState.OPEN ? newCoupDoorState : CoupDoorState.ERROR);
                    }
                }
            }

            @Override
            public boolean cancel() {
                boolean notRunYet = super.cancel();
                Logger.debug("Cancel called after " + (System.currentTimeMillis() - scheduledAtMs) + " millisecond(s) " + (notRunYet ? "(not run yet)" : "(already run)"));
                return notRunYet;
            }
        };

        _timer.schedule(_unconditionalShutdownTimerTask, shutdownAfterMs);
    }

    @Override
    public void close(boolean unconditional) {

        // Ensure we are in OPEN state
        if (!unconditional && getCoupDoorState() != CoupDoorState.OPEN)
        {
            Logger.warn("Coup door is not in open state, will ignore command");
            return;
        }

        int pwmValue =_motorPwnOutput.getPwm();
        if (pwmValue != 0)
        {
            Logger.warn("Coup door is already moving, will ignore command");
            // Motor is already running, something is not right!
            return;
        }

        gotoCoupDoorState(CoupDoorState.CLOSING);

        Logger.debug("Coup door closing initiating");
        _notificationManager.setLEDState(NotificationManager.LEDType.GREEN, true);

        // Add time for X seconds to ensure motor does not run forever by mistake
        scheduleUnconditionalShutdownOfMotor(_doorConfiguration._maximumMotorRuntimeSec * 1000);

        _startLastOperationMs = System.currentTimeMillis();

        // Set motor direction GPIO to "DOWN"
        // Set motor Pwn GPIO to "GO"
        setupMotorDirection(MotorDirection.DOWN);
        _motorPwnOutput.setPwm(1024/(100/_doorConfiguration._motorSpeedPercentage));

        Logger.debug("Coup door closing initiated");


    }

    public void shutdown() {
    }

    public CoupDoorState getCoupDoorState() {
        return updateCoupDoorState();
    }

    public synchronized CoupDoorState updateCoupDoorState()
    {

        if (_coupDoorState == CoupDoorState.UNKNOWN || _coupDoorState == CoupDoorState.ERROR)
        {
            Logger.warn("Coup door in " + _coupDoorState + ", will attempt to read hardware state");

            // Attempt to pull the state from the pins - if not able to
            // we set it to error

            CoupDoorState coupDoorState = CoupDoorState.UNKNOWN;

            final PinState openState = _openDigitalInput.getState();
            final PinState closedState = _closedDigitalInput.getState();

            if (openState.isHigh()) {
                coupDoorState = CoupDoorState.OPEN;
            }
            if (closedState.isHigh()) {
                coupDoorState = CoupDoorState.CLOSED;
            }

            if (coupDoorState == CoupDoorState.UNKNOWN) {

                Logger.debug("Coup door state still unknown, will attempt to read motor state");

                final boolean pwmState = _motorPwnOutput.getPwm() > 0;
                if (pwmState) {
                    Logger.debug("Motor is running, resolving direction");
                    // Motor is running
                    MotorDirection motorDirection = resolveMotorDirection();
                    switch (motorDirection) {
                        case UP:
                            coupDoorState = CoupDoorState.OPENING;
                            break;
                        case DOWN:
                            coupDoorState = CoupDoorState.CLOSING;
                            break;
                    }
                }
            }

            if (coupDoorState == CoupDoorState.UNKNOWN)
            {
                Logger.warn("Coup door in " + coupDoorState + " after reading hardware");
                coupDoorState = CoupDoorState.ERROR;
            }

            Logger.debug("Coup door in " + coupDoorState + " after reading hardware, will update global state");

            gotoCoupDoorState(coupDoorState);
        }

        Logger.debug("Coup door in state: " + _coupDoorState);


        return _coupDoorState;
    }

    private MotorDirection resolveMotorDirection() {

        final MotorDirection motorDirection;
        if (_doorConfiguration._motorDirectionGPIOHighIsUp) {
            motorDirection = _motorDirectionDigitalOutput.isHigh() ? MotorDirection.UP : MotorDirection.DOWN;
        }
        else {
            motorDirection = _motorDirectionDigitalOutput.isHigh() ? MotorDirection.DOWN : MotorDirection.UP;
        }
        return motorDirection;
    }

    public CoopDoorStatusMessage.HardwareState getCoupDoorHardwareState()
    {
        CoopDoorStatusMessage.HardwareState hardwareState = new CoopDoorStatusMessage.HardwareState();
        hardwareState._openedSensorPinState = _openDigitalInput.getState().isHigh() ? HIGH : LOW;
        hardwareState._closedSensorPinState = _closedDigitalInput.getState().isHigh() ? HIGH : LOW;
        return hardwareState;
    }

    public List<CoopDoorStatusMessage.CoupDoorStateTransition> getStateTransitions()
    {
        return new ArrayList<>(_coupDoorStateTransitions);
    }

}
