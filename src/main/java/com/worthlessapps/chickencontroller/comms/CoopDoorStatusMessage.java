package com.worthlessapps.chickencontroller.comms;

import com.google.gson.annotations.SerializedName;
import com.worthlessapps.chickencontroller.CoopDoorImpl;
import com.worthlessapps.chickencontroller.config.ChickenCoopConfig;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import java.util.List;

public class CoopDoorStatusMessage {

    public enum CommandStatus
    {
        OK, ERROR
    }

    public enum PinState
    {
        HIGH, LOW
    }

    public enum State
    {
        UNKNOWN, OPENING, OPEN, CLOSING, CLOSED, ERROR
    }


    public static class SchedulerState
    {
        @SerializedName("suspended")
        public boolean _suspended;
        @SerializedName("suspendedUntilMs")
        public Long _suspendedUntilMs;
        @SerializedName("suspendedUntil")
        public String _suspendedUntil;

        @SerializedName("nextOpenClose")
        public OpenCloseSchedule _nextOpenClose;

        @SerializedName("shouldBeClosed")
        public boolean _shouldBeClosed;

        public static class OpenCloseSchedule
        {
            @SerializedName("lastOpenActionMs")
            public Long _lastOpenActionMs;
            @SerializedName("lastOpenAction")
            public String _lastOpenAction;

            @SerializedName("nextOpenActionMs")
            public Long _nextOpenActionMs;
            @SerializedName("nextOpenAction")
            public String _nextOpenAction;

            @SerializedName("lastCloseActionMs")
            public Long _lastCloseActionMs;
            @SerializedName("lastCloseAction")
            public String _lastCloseAction;

            @SerializedName("nextCloseActionMs")
            public Long _nextCloseActionMs;
            @SerializedName("nextCloseAction")
            public String _nextCloseAction;

            public OpenCloseSchedule(Long lastOpenActionMs, Long nextOpenActionMs, Long lastCloseActionMs, Long nextCloseActionMs) {
                
                _lastOpenActionMs = lastOpenActionMs;
                _nextOpenActionMs = nextOpenActionMs;
                _lastCloseActionMs = lastCloseActionMs;
                _nextCloseActionMs = nextCloseActionMs;

                final DateTimeFormatter fmt = ISODateTimeFormat.dateTime();

                DateTime dt = null;
                if (_nextOpenActionMs != null) {
                    dt = new DateTime(_nextOpenActionMs);
                    _nextOpenAction = fmt.print(dt);
                }

                if (_lastOpenActionMs != null) {
                    dt = new DateTime(_lastOpenActionMs);
                    _lastOpenAction = fmt.print(dt);
                }

                if (_nextCloseActionMs != null) {
                    dt = new DateTime(_nextCloseActionMs);
                    _nextCloseAction = fmt.print(dt);
                }

                if (_lastCloseActionMs != null) {
                    dt = new DateTime(_lastCloseActionMs);
                    _lastCloseAction = fmt.print(dt);
                }

            }
        }

        public static enum SchedulerAction
        {
            OPEN, CLOSE
        }

        @SerializedName("suspendedActions")
        public List<SchedulerAction> _suspendedActions;

        public SchedulerState(boolean suspended, DateTime suspendedUntilDateTime, OpenCloseSchedule nextOpenClose) {
            _suspended = suspended;
            _nextOpenClose = nextOpenClose;
            if (suspendedUntilDateTime != null) {
                _suspendedUntilMs = suspendedUntilDateTime.getMillis();
                final DateTimeFormatter fmt = ISODateTimeFormat.dateTime();
                _suspendedUntil = fmt.print(suspendedUntilDateTime);
            }
        }
    }

    public static class HardwareState
    {
        @SerializedName("closedSensorPinState")
        public PinState _closedSensorPinState;
        @SerializedName("openedSensorPinState")
        public PinState _openedSensorPinState;
    }

    @SerializedName("commandStatusMessage")
    public String _commandStatusMsg;

    @SerializedName("commandStatus")
    public CommandStatus _commandStatus;

    @SerializedName("doorState")
    public State _currentDoorState;

    @SerializedName("stateTransitions")
    public List<CoupDoorStateTransition> _coupDoorStateTransitions;

    public static class CoupDoorStateTransition
    {
        @SerializedName("from")
        public CoopDoorImpl.CoupDoorState _from;
        @SerializedName("to")
        public CoopDoorImpl.CoupDoorState _to;

        @SerializedName("date")
        public String _date;

        @SerializedName("timestampMs")
        public long _timestampMs;

        @SerializedName("diffFromLastMs")
        public long _diffFromLastMs;

        public CoupDoorStateTransition(CoopDoorImpl.CoupDoorState coupDoorState, CoopDoorImpl.CoupDoorState newCoupDoorState, long timestampMs, long diffFromLastMs) {
            _from = coupDoorState;
            _to = newCoupDoorState;
            _timestampMs = timestampMs;
            _diffFromLastMs = diffFromLastMs;

            final DateTime dt = new DateTime(timestampMs);
            final DateTimeFormatter fmt = ISODateTimeFormat.dateTime();
            _date= fmt.print(dt);
        }
    }

    public enum EventCategory
    {
        NOTIFICATION, ERROR
    }

    public static class Event
    {
        @SerializedName("date")
        public String _date;

        @SerializedName("timestamp")
        public long _timestamp;
        @SerializedName("category")
        public EventCategory _category;
        @SerializedName("message")
        public String _message;

    }

    @SerializedName("events")
    public List<Event> _events;

    @SerializedName("coupDoorHardwareState")
    public HardwareState _hardwareState;

    @SerializedName("currentTime")
    public String _currentTime;

    @SerializedName("uptimeSec")
    public Long _uptimeSec;

    @SerializedName("upSince")
    public String _upSince;

    @SerializedName("schedulerState")
    public SchedulerState _schedulerState;

    @SerializedName("currentConfiguration")
    public ChickenCoopConfig _currentConfiguration;

}
