package com.worthlessapps.chickencontroller.comms;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.worthlessapps.chickencontroller.CoopDoor;
import com.worthlessapps.chickencontroller.CoopDoorImpl;
import com.worthlessapps.chickencontroller.config.ChickenCoopConfig;
import com.worthlessapps.chickencontroller.events.EventManager;
import com.worthlessapps.chickencontroller.scheduling.CoopScheduler;
import com.worthlessapps.chickencontroller.utils.Logger;
import com.worthlessapps.chickencontroller.utils.TimeHelper;
import org.eclipse.jetty.security.ConstraintMapping;
import org.eclipse.jetty.security.ConstraintSecurityHandler;
import org.eclipse.jetty.security.HashLoginService;
import org.eclipse.jetty.security.LoginService;
import org.eclipse.jetty.security.authentication.BasicAuthenticator;
import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.util.security.Constraint;
import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.ReadableDuration;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.Collections;
import java.util.regex.Matcher;

public class HTTPServer {

    private final CoopDoor _coopDoor;
    private CoopScheduler _coopScheduler;
    private EventManager _eventManager;
    private final ChickenCoopConfig _chickenCoopConfig;
    private Server server;

    private static final long START_TIME_SERVER = System.currentTimeMillis();

    public HTTPServer(ChickenCoopConfig chickenCoopConfig, CoopDoor coopDoor, CoopScheduler coopScheduler, EventManager eventManager) {
        _chickenCoopConfig = chickenCoopConfig;
        _coopDoor = coopDoor;
        _coopScheduler = coopScheduler;
        _eventManager = eventManager;
    }

    public void initialize() {
        try {
            server = new Server();
            ServerConnector connector = new ServerConnector(server);
            connector.setPort(_chickenCoopConfig._serverConfiguration._port);
            connector.setHost(_chickenCoopConfig._serverConfiguration._host);
            server.setConnectors(new Connector[]{connector});

            String realmResourceName = "etc/realm.properties";
            ClassLoader classLoader = HTTPServer.class.getClassLoader();
            URL realmProps = classLoader.getResource(realmResourceName);
            if (realmProps == null)
                throw new FileNotFoundException("Unable to find " + realmResourceName);

            LoginService loginService = new HashLoginService("MyRealm",
                    realmProps.toExternalForm());

            server.addBean(loginService);

            // A security handler is a jetty handler that secures content behind a
            // particular portion of a url space. The ConstraintSecurityHandler is a
            // more specialized handler that allows matching of urls to different
            // constraints. The server sets this as the first handler in the chain,
            // effectively applying these constraints to all subsequent handlers in
            // the chain.
            ConstraintSecurityHandler security = new ConstraintSecurityHandler();
            server.setHandler(security);

            // This constraint requires authentication and in addition that an
            // authenticated user be a member of a given set of roles for
            // authorization purposes.
            Constraint constraint = new Constraint();
            constraint.setName("auth");
            constraint.setAuthenticate(true);
            constraint.setRoles(new String[]{"user", "admin"});

            // Binds a url pattern with the previously created constraint. The roles
            // for this constraint mapping are mined from the Constraint itself
            // although methods exist to declare and bind roles separately as well.
            ConstraintMapping mapping = new ConstraintMapping();
            mapping.setPathSpec("/*");
            mapping.setConstraint(constraint);

            // First you see the constraint mapping being applied to the handler as
            // a singleton list, however you can passing in as many security
            // constraint mappings as you like so long as they follow the mapping
            // requirements of the servlet api. Next we set a BasicAuthenticator
            // instance which is the object that actually checks the credentials
            // followed by the LoginService which is the store of known users, etc.
            security.setConstraintMappings(Collections.singletonList(mapping));
            security.setAuthenticator(new BasicAuthenticator());
            security.setLoginService(loginService);

            ServletContextHandler context = new ServletContextHandler();
            context.setContextPath("/");

            // Option 1: Direct servlet instantiation and ServletHolder
            CoupDoorServlet coupDoorServlet = new CoupDoorServlet(_chickenCoopConfig, _coopDoor);
            ServletHolder servletHolder = new ServletHolder(coupDoorServlet);
            context.addServlet(servletHolder, "/fc3/*");

            security.setHandler(context);

            server.start();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void join() {
        try {
            server.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void shutdown() {
        try {
            server.stop();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class CoupDoorServlet extends HttpServlet {
        private ChickenCoopConfig _chickenCoopConfig;
        private CoopDoor _coopDoor;

        public CoupDoorServlet(ChickenCoopConfig chickenCoopConfig, CoopDoor coopDoor) {
            _chickenCoopConfig = chickenCoopConfig;
            _coopDoor = coopDoor;
        }

        @Override
        protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
            String path = request.getRequestURI();

            CoopDoorStatusMessage.CommandStatus commandStatus = CoopDoorStatusMessage.CommandStatus.OK;
            String commandStatusMsg = null;

            CoopDoorStatusMessage coopDoorStatusMessage = new CoopDoorStatusMessage();

            Logger.debug("Request for path: " + path);

            boolean wasCommand = false;
            if (path.equals("/fc3/door/open")) {
                wasCommand = true;

                boolean unconditional = false;
                String paramValue = request.getParameter("unconditional");
                if (paramValue != null) {
                    unconditional = Boolean.parseBoolean(paramValue);
                }

                _coopDoor.open(unconditional);
                CoopDoor.CoupDoorState coupDoorState = _coopDoor.getCoupDoorState();
                if (coupDoorState != CoopDoor.CoupDoorState.OPENING) {
                    commandStatus = CoopDoorStatusMessage.CommandStatus.OK;
                    commandStatusMsg = "Coup door state is not 'OPENING' as expected: " + coupDoorState;
                }
            } else if (path.equals("/fc3/door/close")) {
                wasCommand = true;

                boolean unconditional = false;
                String paramValue = request.getParameter("unconditional");
                if (paramValue != null) {
                    unconditional = Boolean.parseBoolean(paramValue);
                }

                _coopDoor.close(unconditional);
                CoopDoor.CoupDoorState coupDoorState = _coopDoor.getCoupDoorState();
                if (coupDoorState != CoopDoor.CoupDoorState.CLOSING) {
                    commandStatus = CoopDoorStatusMessage.CommandStatus.ERROR;
                    commandStatusMsg = "Coup door state is not 'CLOSING' as expected: " + coupDoorState;
                }
            } else if (path.equals("/fc3/status")) {

                DateTime dt = new DateTime();
                DateTimeFormatter fmt = ISODateTimeFormat.dateTime();
                coopDoorStatusMessage._currentTime = fmt.print(dt);

                coopDoorStatusMessage._uptimeSec = (System.currentTimeMillis() - START_TIME_SERVER) / 1000;
                dt = new DateTime(START_TIME_SERVER);
                coopDoorStatusMessage._upSince = fmt.print(dt);

                coopDoorStatusMessage._schedulerState = _coopScheduler.getCoopSchedulerState();
                coopDoorStatusMessage._hardwareState = ((CoopDoorImpl)_coopDoor).getCoupDoorHardwareState();
                coopDoorStatusMessage._coupDoorStateTransitions = ((CoopDoorImpl)_coopDoor).getStateTransitions();
                coopDoorStatusMessage._currentConfiguration = _chickenCoopConfig;

                coopDoorStatusMessage._events = _eventManager.getEvents();

            } else if (path.equals("/fc3/scheduler/suspend")) {
                wasCommand = true;
                if (!_coopScheduler.isSuspended()) {
                    final String duration = request.getParameter("duration");
                    final String[] actions = request.getParameterValues("action");

                    Long suspendUntilMs = null;
                    CoopScheduler.CoopSchedulerAction[] suspendedActions = new CoopScheduler.CoopSchedulerAction[actions != null ? actions.length : 0];

                    if (actions != null) {
                        int index = 0;
                        for (String action : actions) {
                            try {
                                suspendedActions[index++] = CoopScheduler.CoopSchedulerAction.valueOf(action);
                            } catch (IllegalArgumentException e) {
                                commandStatus = CoopDoorStatusMessage.CommandStatus.ERROR;
                                commandStatusMsg = "Invalid duration: " + duration;
                                break;
                            }
                        }
                    }

                    if (commandStatus == CoopDoorStatusMessage.CommandStatus.OK) {
                        if (duration != null) {
                            final Matcher durationMatcher = TimeHelper.createDurationMatcher(duration);
                            if (durationMatcher.matches()) {
                                char unit = 'm';
                                if (durationMatcher.groupCount() == 2) {
                                    // We have a unit!
                                    unit = durationMatcher.group(2).charAt(0);
                                }

                                final ReadableDuration readableDuration;
                                switch (unit) {
                                    case 'h':
                                        readableDuration = Duration.standardHours(Integer.parseInt(durationMatcher.group(1)));
                                        break;
                                    case 'd':
                                        readableDuration = Duration.standardDays(Integer.parseInt(durationMatcher.group(1)));
                                        break;
                                    case 'm':
                                    default:
                                        readableDuration = Duration.standardMinutes(Integer.parseInt(durationMatcher.group(1)));
                                        break;
                                }

                                suspendUntilMs = DateTime.now().plus(readableDuration).getMillis();
                            } else {
                                commandStatus = CoopDoorStatusMessage.CommandStatus.ERROR;
                                commandStatusMsg = "Invalid duration: " + duration;
                            }
                        }

                        if (commandStatus == CoopDoorStatusMessage.CommandStatus.OK) {
                            if (suspendUntilMs != null) {
                                _coopScheduler.suspendUntil(suspendUntilMs, suspendedActions);
                            } else {
                                _coopScheduler.suspend(suspendedActions);
                            }
                        }
                    }
                }
                else
                {
                    commandStatus = CoopDoorStatusMessage.CommandStatus.ERROR;
                    commandStatusMsg = "Coop scheduler is already suspended";
                }
            } else if (path.equals("/fc3/scheduler/resume")) {
                wasCommand = true;
                if (_coopScheduler.isSuspended()) {
                    _coopScheduler.resume();
                }
                else
                {
                    commandStatus = CoopDoorStatusMessage.CommandStatus.ERROR;
                    commandStatusMsg = "Coop scheduler not suspended";
                }
            }

            CoopDoor.CoupDoorState coupDoorState = _coopDoor.getCoupDoorState();
            coopDoorStatusMessage._currentDoorState = CoopDoorStatusMessage.State.values()[coupDoorState.ordinal()];

            if (wasCommand) {
                coopDoorStatusMessage._commandStatus = commandStatus;
                coopDoorStatusMessage._commandStatusMsg = commandStatusMsg;
            }

            Gson gson = new GsonBuilder().setPrettyPrinting().create();

            String json = gson.toJson(coopDoorStatusMessage);

            response.setStatus(HttpServletResponse.SC_OK);
            response.setContentType("application/json");
            response.setCharacterEncoding("utf-8");
            response.getWriter().write(json);

        }

    }

}
