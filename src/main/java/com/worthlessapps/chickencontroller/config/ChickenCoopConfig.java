package com.worthlessapps.chickencontroller.config;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

public class ChickenCoopConfig {

    public static class HardwareConfiguration
    {
        @SerializedName("ledConfiguration")
        public LEDConfiguration _ledConfiguration;

        @SerializedName("doorConfiguration")
        public DoorConfiguration _doorConfiguration;

        public static class LEDConfiguration
        {
            @SerializedName("greenLEDGPIO")
            public int _greenLEDGPIO;
            @SerializedName("yellowEDGPIO")
            public int _yellowLEDGPIO;
            @SerializedName("redLEDGPIO")
            public int _redLEDGPIO;
        }

        public static class DoorConfiguration
        {
            @SerializedName("openSwitchGPIO")
            public int _openSwitchGPIO;
            @SerializedName("closedSwitchGPIO")
            public int _closedSwitchGPIO;

            @SerializedName("hardwareSwitch")
            public int _hardwareSwitchGPIO;

            @SerializedName("motorPwnGPIO")
            public int _motorPwnGPIO;
            @SerializedName("motorDirectionGPIO")
            public int _motorDirectionGPIO;
            @SerializedName("motorDirectionGPIOHighIsUp")
            public boolean _motorDirectionGPIOHighIsUp;

            @SerializedName("motorSpeedPercentage")
            public int _motorSpeedPercentage = 75;
            @SerializedName("maximumMotorRuntimeSec")
            public int _maximumMotorRuntimeSec = 10;

            @SerializedName("runMotorExtraMs")
            public int _runMotorExtraMs = 0;
        }

    }

    public static class Scheduling
    {
        @SerializedName("open")
        public String _open;
        @SerializedName("close")
        public String _close;
    }

    public static class IPStackConfiguration
    {
        @SerializedName("apiKey")
        public String _apiKey;
    }

    public static class Location
    {
        @SerializedName("latitude")
        public double _latitude;
        @SerializedName("longitude")
        public double _longitude;
    }

    public static class ServerConfiguration
    {
        @SerializedName("port")
        public int _port = 10420;
        @SerializedName("host")
        public String _host = "localhost";

        @SerializedName("location")
        public Location _location;

        @SerializedName("ipstackConfiguration")
        public IPStackConfiguration _ipStackConfiguration;

        @SerializedName("hardwareConfiguration")
        public HardwareConfiguration _hardwareConfiguration;

        @SerializedName("scheduling")
        public Scheduling _scheduling;

    }

    @SerializedName("serverConfiguration")
    public ServerConfiguration _serverConfiguration;

    public static void main(String[] args) {
        ChickenCoopConfig chickenCoopConfig = new ChickenCoopConfig();
        chickenCoopConfig._serverConfiguration = new ServerConfiguration();
        chickenCoopConfig._serverConfiguration._host = "localhost";
        chickenCoopConfig._serverConfiguration._port = 12420;

        HardwareConfiguration hardwareConfiguration = new HardwareConfiguration();
        chickenCoopConfig._serverConfiguration._hardwareConfiguration = hardwareConfiguration;

        HardwareConfiguration.DoorConfiguration doorConfiguration = new HardwareConfiguration.DoorConfiguration();
        hardwareConfiguration._doorConfiguration = doorConfiguration;

        doorConfiguration._openSwitchGPIO = 5;
        doorConfiguration._closedSwitchGPIO = 6;
        doorConfiguration._motorDirectionGPIO = 4;
        doorConfiguration._motorPwnGPIO = 1;
        doorConfiguration._hardwareSwitchGPIO = 25;

        HardwareConfiguration.LEDConfiguration ledConfiguration = new HardwareConfiguration.LEDConfiguration();
        hardwareConfiguration._ledConfiguration = ledConfiguration;

        ledConfiguration._greenLEDGPIO = 0;
        ledConfiguration._yellowLEDGPIO = 2;
        ledConfiguration._redLEDGPIO = 3;


        Gson gson = new Gson();
        String s = gson.toJson(chickenCoopConfig);
        System.out.println("s = " + s);
    }

}
