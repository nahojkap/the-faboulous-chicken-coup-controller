package com.worthlessapps.chickencontroller.cli;

import com.google.gson.Gson;
import com.pi4j.io.gpio.GpioFactory;
import com.worthlessapps.chickencontroller.CoopDoorImpl;
import com.worthlessapps.chickencontroller.comms.CoopDoorStatusMessage;
import com.worthlessapps.chickencontroller.events.EventManager;
import com.worthlessapps.chickencontroller.events.EventManagerImpl;
import com.worthlessapps.chickencontroller.location.IPStackLocationResolver;
import com.worthlessapps.chickencontroller.notifications.NotificationManager;
import com.worthlessapps.chickencontroller.comms.HTTPServer;
import com.worthlessapps.chickencontroller.config.ChickenCoopConfig;
import com.worthlessapps.chickencontroller.notifications.NotificationManagerImpl;
import com.worthlessapps.chickencontroller.scheduling.CoopSchedulerImpl;
import com.worthlessapps.chickencontroller.utils.Logger;
import picocli.CommandLine;
import picocli.CommandLine.*;

import java.io.File;
import java.io.FileReader;
import java.util.concurrent.Callable;

@Command(name = "chickencoupctrl", mixinStandardHelpOptions = true, version = "0.1", description = "Controls the Chicken Coup")
public class FabulousChickenCoupController implements Callable<Integer> {

    public static enum ChickenControllerCommand {
        OPEN, CLOSE, STATUS
    }

    @ArgGroup(exclusive = true)
    ModeOptions _modeOptions;

    static class ModeOptions {

        @Option(names = {"-s", "--server"}, description = "Start as server application", defaultValue = "false")
        private boolean _server = false;

        @Option(names = {"-c", "--client"}, description = "Start as server application", defaultValue = "true")
        private boolean _client = false;

    }

    @Option(names = {"-f", "--config"}, description = "Specifies the configuration file to use", defaultValue = "~/.chickencoupctrl/config.json")
    private String _configurationFile;

    @Option(names = {"--cmd"}, description = "Command to send")
    ChickenControllerCommand command;

    @Option(names = {"--host"}, description = "Specifies the host to connect to")
    private String _host = "localhost";

    @Option(names = {"-p", "--post"}, defaultValue = "10042", description = "Specifies the port to connect to ")
    private int _port = 10042;

    public static void main(String args[]) {
        int exitCode = new CommandLine(new FabulousChickenCoupController()).execute(args);
        System.exit(exitCode);
    }

    HTTPServer server = null;
    EventManager eventManager = new EventManagerImpl();

    @Override
    public Integer call() throws Exception {

        if (_modeOptions._server)
        {
            Runtime.getRuntime().addShutdownHook(new Thread()
            {
                public void run()
                {
                    eventManager.sendEvent(CoopDoorStatusMessage.EventCategory.NOTIFICATION,"Shutdown initiated");
                    Logger.info("Shutdown initiated");
                    if (server != null)
                    {
                        server.shutdown();
                    }
                }
            });
            
            eventManager.sendEvent(CoopDoorStatusMessage.EventCategory.NOTIFICATION,"Chicken Coup Controller Starting");

            Logger.info("\n" +
                    "                    ,.\n" +
                    "                   (\\(\\)\n" +
                    "   ,_              ;  o >\n" +
                    "    {`-.          /  (_) \n" +
                    "    `={\\`-._____/`   |\n" +
                    "   __`-{ /    -=`\\   |___\n" +
                    "  (___`={__-=_=__/___/___)\n" +
                    "   \\=I=I=I=I=I=I=I=I=I=I/\n" +
                    "    \\=I=I=I=I=I=I=I=I=I/\n" +
                    "     \\=I=I=I=I=I=I=I=I/\n" +
                    "  jgs \\=I=I=I=I=I=I=I/\n" +
                    "       `------------'");
            // Starting in server mode
            Logger.info("Will load configuration from " + _configurationFile);

            String path = _configurationFile.replaceFirst("^~", System.getProperty("user.home"));
            File f = new File(path);
            if (!f.exists())
            {
                System.out.println("File " + path + " does not exist");
                return null;
            }

            Gson gson = new Gson();
            ChickenCoopConfig chickenCoopConfig = gson.fromJson(new FileReader(f), ChickenCoopConfig.class);

            ChickenCoopConfig.HardwareConfiguration.LEDConfiguration ledConfiguration = chickenCoopConfig._serverConfiguration._hardwareConfiguration._ledConfiguration;
            NotificationManagerImpl notificationManager = new NotificationManagerImpl(ledConfiguration);
            notificationManager.initialize();

            notificationManager.setLEDState(NotificationManager.LEDType.RED, true);
            notificationManager.blinkLED(NotificationManager.LEDType.YELLOW,0, 250,250);

            // Validate configuration!!!
            ChickenCoopConfig.HardwareConfiguration hardwareConfiguration = chickenCoopConfig._serverConfiguration._hardwareConfiguration;

            IPStackLocationResolver locationResolver = new IPStackLocationResolver(chickenCoopConfig);
            locationResolver.initialize();

            CoopDoorImpl coopDoor = new CoopDoorImpl(notificationManager,eventManager);

            ChickenCoopConfig.HardwareConfiguration.DoorConfiguration doorConfiguration = hardwareConfiguration._doorConfiguration;

            coopDoor.initialize(doorConfiguration);

            eventManager.sendEvent(CoopDoorStatusMessage.EventCategory.NOTIFICATION,"Coup Door initialized");
            Logger.info("Coup door initialized");

            CoopSchedulerImpl coopScheduler = new CoopSchedulerImpl(chickenCoopConfig, coopDoor, notificationManager, eventManager, locationResolver);
            coopScheduler.initialize();

            server = new HTTPServer(chickenCoopConfig, coopDoor, coopScheduler, eventManager);
            server.initialize();

            Logger.info("Server started on " + chickenCoopConfig._serverConfiguration._port);
            eventManager.sendEvent(CoopDoorStatusMessage.EventCategory.NOTIFICATION,"Server started");

            notificationManager.stopBlinkLED(NotificationManager.LEDType.YELLOW);
            notificationManager.setLEDState(NotificationManager.LEDType.YELLOW, true);

            server.join();

            notificationManager.shutdown();

        }
        else
        {

        }

        GpioFactory.getInstance().shutdown();

        return 0;
    }
}