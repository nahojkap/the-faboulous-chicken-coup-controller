package com.worthlessapps.chickencontroller.scheduling;

import com.coreoz.wisp.Job;
import com.coreoz.wisp.LongRunningJobMonitor;
import com.coreoz.wisp.Scheduler;
import com.coreoz.wisp.schedule.Schedule;
import com.coreoz.wisp.schedule.Schedules;
import com.worthlessapps.chickencontroller.CoopDoor;
import com.worthlessapps.chickencontroller.comms.CoopDoorStatusMessage;
import com.worthlessapps.chickencontroller.config.ChickenCoopConfig;
import com.worthlessapps.chickencontroller.events.EventManager;
import com.worthlessapps.chickencontroller.location.LocationResolver;
import com.worthlessapps.chickencontroller.notifications.NotificationManager;
import com.worthlessapps.chickencontroller.utils.Logger;
import com.worthlessapps.chickencontroller.utils.TimeHelper;
import org.joda.time.DateTime;

import java.time.Duration;
import java.util.*;
import java.util.regex.Matcher;

/**
 * @author
 */
public class CoopSchedulerImpl implements CoopScheduler
{

    private final EventManager _eventManager;
    private final ChickenCoopConfig _chickenCoopConfig;
    private final CoopDoor _coopDoor;
    private final NotificationManager _notificationManager;

    private final boolean _openSuspended = false;
    private final boolean _closeSuspended = false;

    private final Set<CoopSchedulerAction> _suspendedActions = new HashSet<>();
    private final LocationResolver _locationResolver;
    private boolean _suspended;
    private DateTime _suspendedUntil;
    public Scheduler _scheduler;

    private Job _openDoorJob, _closeDoorJob;

    // FIXME: Needs persistent store to "remember" if coup is suspended

    @FunctionalInterface
    private interface CoopDoorAction
    {
        public void performAction();
    }

    private class CoopDoorActionExecutor implements Runnable
    {
        private final CoopSchedulerAction _coopSchedulerAction;
        private final CoopDoorAction _action;

        public CoopDoorActionExecutor(final CoopSchedulerAction coopSchedulerAction, final CoopDoorAction action)
        {
            _coopSchedulerAction = coopSchedulerAction;
            _action = action;
        }

        @Override
        public void run()
        {

            validateSuspensionStatus();

            if (!CoopSchedulerImpl.this.isActionSuspended(_coopSchedulerAction))
            {
                Logger.info("Initiating action '" + _coopSchedulerAction + "'");
                _action.performAction();
            }
            else
            {
                Logger.info("Action for '" + _coopSchedulerAction + "' is suspended, will skip it");
            }

        }
    }

    private void validateSuspensionStatus()
    {
        if (_suspended)
        {
            if (_suspendedUntil != null)
            {
                if (_suspendedUntil.isBefore(DateTime.now()))
                {
                    Logger.info("Suspension has been lifted ");
                    _suspended = false;
                    _suspendedUntil = null;
                    _suspendedActions.clear();
                }
                else
                {
                    long suspendedForAdditionalMs = _suspendedUntil.getMillis() - DateTime.now().getMillis();
                    Logger.debug("Suspended for an additional " + suspendedForAdditionalMs + " millisecond(s)");
                }
            }
        }

    }

    public CoopSchedulerImpl(final ChickenCoopConfig chickenCoopConfig,
                             final CoopDoor coopDoor,
                             final NotificationManager notificationManager,
                             final EventManager eventManager, LocationResolver locationResolver)
    {

        _chickenCoopConfig = chickenCoopConfig;
        _coopDoor = coopDoor;
        _notificationManager = notificationManager;
        _eventManager = eventManager;
        _locationResolver = locationResolver;
    }

    public void shutdown()
    {
        _scheduler.gracefullyShutdown();
    }

    public void initialize()
    {
        _scheduler = new Scheduler();
        _scheduler.schedule(
                "Long running job monitor",
                new LongRunningJobMonitor(_scheduler),
                Schedules.fixedDelaySchedule(Duration.ofMinutes(1))
        );

        if (_chickenCoopConfig._serverConfiguration._scheduling != null)
        {
            if (_chickenCoopConfig._serverConfiguration._scheduling._close == null || _chickenCoopConfig._serverConfiguration._scheduling._open == null)
            {
                Logger.error("Scheduling details does not specify both open and close information");
            }
            else
            {
                Logger.info("Creating 'OPEN' schedule");

                _openDoorJob = _scheduler.schedule("OpenDoorJob", new CoopDoorActionExecutor(
                        CoopSchedulerAction.OPEN,() -> {
                            if (!this.isActionSuspended(CoopSchedulerAction.OPEN))
                            {
                                _coopDoor.open(false);
                            }
                        }),
                        createScheduleFromEntry(_chickenCoopConfig._serverConfiguration._scheduling._open)
                );


                Logger.info("Creating 'CLOSE' schedule");
                _closeDoorJob = _scheduler.schedule("CloseDoorJob", new CoopDoorActionExecutor(
                        CoopSchedulerAction.CLOSE, () -> {
                            if (!this.isActionSuspended(CoopSchedulerAction.CLOSE))
                            {
                                _coopDoor.close(false);
                            }
                        }),
                        createScheduleFromEntry(_chickenCoopConfig._serverConfiguration._scheduling._close)
                );

            }
            
        }
        else
        {
            Logger.warn("No scheduling information provided, in manual mode");
        }

        if (shouldBeClosed())
        {
            Logger.info("Coup door should be closed, will do just that");
            _coopDoor.close(false);
        }

        Logger.info("Scheduler started");
    }

    private boolean isActionSuspended(CoopSchedulerAction action)
    {
        validateSuspensionStatus();
        return _suspendedActions.contains(action);
    }

    Schedule createScheduleFromEntry(String schedule)
    {

        // sunrise/sunset +- Nm
        final Matcher matcher = TimeHelper.createSunriseSunsetMatcher(schedule);
        if (matcher.matches())
        {
            // We have a match
            Logger.info("Creating schedule from sunrise/sunset entry: " + schedule);

            final boolean sunrise = matcher.group(1).equalsIgnoreCase("sunrise");
            final int multiplier = matcher.group(2).equalsIgnoreCase("-") ? -1 : 1;
            final int offsetMs = multiplier * (Integer.parseInt(matcher.group(3)) * 60 * 1000);

            Logger.debug("Will schedule entry based on: " + matcher.group(1) + " " + (multiplier == 1 ? "+" : "-") + " " + offsetMs);
            return new SunEventBasedCoupSchedule(_locationResolver, sunrise, offsetMs);
        }
        else
        {
            Logger.info("Creating schedule from simple time: " + schedule);
            final Matcher timeMatcher = TimeHelper.createTimeMatcher(schedule);
            if (timeMatcher.matches())
            {
                return new SimpleTimeBasedCoupSchedule(schedule);
            }
            else
            {
                throw new IllegalStateException("Unhandled schedule: " + schedule);
            }
        }
    }

    public boolean shouldBeClosed()
    {
        return shouldBeClosed(DateTime.now());
    }

    public boolean shouldBeClosed(DateTime now)
    {
        if (_closeDoorJob != null)
        {
            // We are in automatic mode

            // Calculate the last close event
            final AbstractCoupSchedule closeSchedule = (AbstractCoupSchedule) _closeDoorJob.schedule();
            DateTime closingAt = new DateTime(closeSchedule.calculateNextEvent(now));

            final AbstractCoupSchedule openSchedule = (AbstractCoupSchedule) _openDoorJob.schedule();
            DateTime openingAt = new DateTime(openSchedule.calculateNextEvent(now));

            if (openingAt.isBefore(closingAt))
            {
                return openingAt.isAfter(now);
            }
            return closingAt.isBefore(now);
        }
        return false;
    }

    @Override
    public boolean isSuspended(CoopSchedulerAction... actions)
    {
        Set<CoopSchedulerAction> actionsToCheck = new HashSet<>();
        if (actions == null || actions.length == 0)
        {
            actionsToCheck.addAll(CoopScheduler.ALL_ACTIONS);
        }
        else
        {
            actionsToCheck.addAll(Arrays.asList(actions));
        }

        return _suspendedActions.containsAll(actionsToCheck);

    }

    @Override
    public CoopDoorStatusMessage.SchedulerState getCoopSchedulerState()
    {

        final Long nextCloseDoorActionMs = _closeDoorJob != null ? _closeDoorJob.nextExecutionTimeInMillis() : null;
        final Long lastCloseDoorActionMs = _closeDoorJob != null ? _closeDoorJob.lastExecutionStartedTimeInMillis() : null;
        final Long nextOpenDoorActionMs = _openDoorJob != null ? _openDoorJob.nextExecutionTimeInMillis() : null;
        final Long lastOpenDoorActionMs = _openDoorJob != null ? _openDoorJob.lastExecutionStartedTimeInMillis() : null;

        final CoopDoorStatusMessage.SchedulerState.OpenCloseSchedule nextOpenClose = new CoopDoorStatusMessage.SchedulerState.OpenCloseSchedule(lastOpenDoorActionMs, nextOpenDoorActionMs, lastCloseDoorActionMs, nextCloseDoorActionMs);

        validateSuspensionStatus();

        CoopDoorStatusMessage.SchedulerState schedulerState = new CoopDoorStatusMessage.SchedulerState(_suspended, _suspended ? _suspendedUntil : null, nextOpenClose);

        schedulerState._suspendedActions = new ArrayList<>();
        for (final CoopSchedulerAction coopSchedulerAction : _suspendedActions)
        {
            schedulerState._suspendedActions.add(CoopDoorStatusMessage.SchedulerState.SchedulerAction.values()[coopSchedulerAction.ordinal()]);
        }

        schedulerState._shouldBeClosed = shouldBeClosed();

        return schedulerState;
    }

    @Override
    public void suspendUntil(long timestampMs, CoopSchedulerAction... actions)
    {
        _suspended = true;
        _suspendedUntil = new DateTime(timestampMs);
    }

    @Override
    public void suspend(CoopSchedulerAction... actions)
    {
        Set<CoopSchedulerAction> actionsToSuspend = new HashSet<>();
        if (actions == null || actions.length == 0)
        {
            actionsToSuspend.addAll(CoopScheduler.ALL_ACTIONS);
        }
        else
        {
            actionsToSuspend.addAll(Arrays.asList(actions));
        }
        _suspendedActions.addAll(actionsToSuspend);

    }

    @Override
    public void resume(CoopSchedulerAction ...actions)
    {
        // Basically clearing all if no actions specified
        if (actions == null || actions.length == 0)
        {
            _suspendedActions.clear();
        }
        else
        {
            boolean removedSomething = _suspendedActions.removeAll(new HashSet<>(Arrays.asList(actions)));
            if (!removedSomething)
            {
                throw new RuntimeException("Specified actions were not in suspended state");
            }
        }
        _suspendedUntil = null;
    }
}
