package com.worthlessapps.chickencontroller.scheduling;

import com.worthlessapps.chickencontroller.utils.TimeHelper;
import org.joda.time.DateTime;

/**
 * @author
 */
public class SimpleTimeBasedCoupSchedule extends AbstractCoupSchedule
{
    final String _schedule;

    public SimpleTimeBasedCoupSchedule(final String schedule)
    {
        super();
        _schedule = schedule;
    }

    @Override
    public DateTime calculateNextEvent(DateTime now)
    {
        DateTime nextEvent = new DateTime(TimeHelper.addStringifiedTime(now.getMillis(), _schedule));
        if (nextEvent.isBefore(now))
        {
            return nextEvent.plusHours(24);
        }
        return nextEvent;
    }

    public DateTime calculateNextAfterNextEvent(DateTime now)
    {
        return new DateTime(TimeHelper.addStringifiedTime(now.getMillis(), _schedule)).plusHours(24);
    }

}
