package com.worthlessapps.chickencontroller.scheduling;

import com.coreoz.wisp.schedule.Schedule;
import com.worthlessapps.chickencontroller.utils.Logger;
import org.joda.time.DateTime;
import org.joda.time.format.ISODateTimeFormat;

/**
 * @author Johan Lindquist
 */
public abstract class AbstractCoupSchedule implements Schedule
{
    private DateTime _nextEvent;
    private DateTime _nextAfterNextEvent;

    public abstract DateTime calculateNextEvent(DateTime now);
    public abstract DateTime calculateNextAfterNextEvent(DateTime now);

    @Override
    public long nextExecutionInMillis(long currentTimeInMs, int executionsCount, Long lastExecutionEndedTimeInMs)
    {
        Logger.trace("Calculating next exectution time: currentTimeMs=" + currentTimeInMs + "; executionsCount=" + executionsCount + "; lastExecutionEndedTimeInMs=" + (lastExecutionEndedTimeInMs == null ? "null" : "" + lastExecutionEndedTimeInMs));

        try
        {
            if (executionsCount == 0)
            {
                _nextEvent = calculateNextEvent(new DateTime(currentTimeInMs));
            }
            else
            {
                _nextEvent = _nextAfterNextEvent;
            }

            _nextAfterNextEvent = calculateNextAfterNextEvent(_nextEvent);

            final long nextExecutionTimeMs = _nextEvent.getMillis();
            final long nextAfterNextExecutionTimeMs = _nextAfterNextEvent.getMillis();
            
            Logger.trace("Scheduling next event at " + ISODateTimeFormat.dateTime().print(nextExecutionTimeMs) + "  (" + (nextExecutionTimeMs - currentTimeInMs) + " millisecond(s) from now)");
            Logger.trace("Scheduling next after next event at " + ISODateTimeFormat.dateTime().print(nextAfterNextExecutionTimeMs) + "  (" + (nextAfterNextExecutionTimeMs - currentTimeInMs) + " millisecond(s) from now)");

            return nextExecutionTimeMs;

        }
        catch (Exception e)
        {
            Logger.error("Error during scheduling: " + e.getMessage(), e);
            return 0;
        }
    }

}
