package com.worthlessapps.chickencontroller.scheduling;

import com.worthlessapps.chickencontroller.comms.CoopDoorStatusMessage;
import org.joda.time.DateTime;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * @author
 */
public interface CoopScheduler {

    CoopDoorStatusMessage.SchedulerState getCoopSchedulerState();

    enum CoopSchedulerAction {
        OPEN, CLOSE
    }

    public boolean shouldBeClosed(DateTime now);

    Set<CoopSchedulerAction> ALL_ACTIONS = Collections.unmodifiableSet(new HashSet<>(Arrays.asList(CoopSchedulerAction.OPEN, CoopSchedulerAction.CLOSE)));
    boolean isSuspended(CoopSchedulerAction ... actions);
    void suspendUntil(long timestampMs, CoopSchedulerAction ... actions);
    void suspend(CoopSchedulerAction ... actions);
    void resume(CoopSchedulerAction ... actions);
}
