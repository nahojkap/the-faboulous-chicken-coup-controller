package com.worthlessapps.chickencontroller.scheduling;

import com.worthlessapps.chickencontroller.location.LocationResolver;
import com.worthlessapps.chickencontroller.utils.Logger;
import org.joda.time.DateTime;
import org.shredzone.commons.suncalc.SunTimes;

import java.time.ZonedDateTime;

/**
 * @author
 */
public class SunEventBasedCoupSchedule extends AbstractCoupSchedule
{

    LocationResolver _locationResolver;
    boolean _sunrise;
    long _offsetMs;

    public SunEventBasedCoupSchedule(LocationResolver locationResolver, boolean sunrise, long offsetMs)
    {
        super();
        _locationResolver = locationResolver;
        _sunrise = sunrise;
        _offsetMs = offsetMs;
    }

    @Override
    public DateTime calculateNextEvent(DateTime now)
    {
        try
        {
            final LocationResolver.Location location = _locationResolver.resolveLocation().get();

            SunTimes times = SunTimes.compute().on(now.toDate()).at(location._latitude, location._longitude).execute();
            ZonedDateTime zonedDateTime = _sunrise ? times.getRise() : times.getSet();
            final long nextExecutionTimeMs = (zonedDateTime.toEpochSecond() * 1000) + _offsetMs;
            return new DateTime(nextExecutionTimeMs);
        }
        catch (Exception e)
        {
            Logger.error("Error calcualting next event: " + e.getMessage(), e);
            throw new RuntimeException("Error calculating next event: " + e.getMessage(), e);
        }
    }

    public DateTime calculateNextAfterNextEvent(DateTime now)
    {
        return calculateNextEvent(now.plusHours(12));
    }

}
