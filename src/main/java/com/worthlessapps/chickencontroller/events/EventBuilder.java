package com.worthlessapps.chickencontroller.events;

import com.worthlessapps.chickencontroller.comms.CoopDoorStatusMessage;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

public class EventBuilder {

    CoopDoorStatusMessage.Event _event = new CoopDoorStatusMessage.Event();
    public static EventBuilder create()
    {
        return new EventBuilder();
    }

    public EventBuilder category(CoopDoorStatusMessage.EventCategory eventCategory)
    {
        _event._category = eventCategory;
        return this;
    }

    public EventBuilder message(String message)
    {
        _event._message = message;
        return this;
    }

    public CoopDoorStatusMessage.Event createEvent()
    {
        _event._timestamp = System.currentTimeMillis();

        DateTime dt = new DateTime(_event._timestamp);
        DateTimeFormatter fmt = ISODateTimeFormat.dateTime();
        _event._date = fmt.print(dt);

        return _event;
    }

}
