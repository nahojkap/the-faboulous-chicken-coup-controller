package com.worthlessapps.chickencontroller.events;

import com.worthlessapps.chickencontroller.comms.CoopDoorStatusMessage;
import com.worthlessapps.chickencontroller.utils.Logger;

import java.util.*;
import java.util.concurrent.Executors;

public class EventManagerImpl implements EventManager
{

    List<CoopDoorStatusMessage.Event> _events = new LinkedList<>();
    int _maxNumberOfEvents = 50;
    Set<EventListener> _eventListeners = new HashSet<>();

    @Override
    public synchronized void sendEvent(CoopDoorStatusMessage.Event event)
    {

        if (_eventListeners.size() > 0)
        {
            Executors.newSingleThreadExecutor().submit(new Runnable()
            {
                @Override
                public void run()
                {
                    for (Iterator<EventListener> iterator = _eventListeners.iterator(); iterator.hasNext(); )
                    {
                        try
                        {
                            EventListener next = iterator.next();
                            next.onEvent(event);
                        }
                        catch (Exception e)
                        {
                        }
                    }
                }
            });
        }

        _events.add(0, event);
        if (_events.size() > _maxNumberOfEvents)
        {
            _events.remove(_events.size() - 1);
        }
    }

    public void sendEvent(CoopDoorStatusMessage.EventCategory eventCategory, String message)
    {
        sendEvent(EventBuilder.create().category(eventCategory).message(message).createEvent());
    }

    @Override
    public synchronized List<CoopDoorStatusMessage.Event> getEvents()
    {
        return new ArrayList<>(_events);
    }

    public synchronized void clear()
    {
        _events.clear();
    }

    @Override
    public void addEventListener(EventListener eventListener)
    {
        this._eventListeners.add(eventListener);
    }

    @Override
    public void removeEventListener(EventListener eventListener)
    {
        this._eventListeners.remove(eventListener);
    }
}
