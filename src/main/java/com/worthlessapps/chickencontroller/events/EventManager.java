package com.worthlessapps.chickencontroller.events;

import com.worthlessapps.chickencontroller.comms.CoopDoorStatusMessage;

import java.util.List;

public interface EventManager {

    public interface EventListener {
        public void onEvent(CoopDoorStatusMessage.Event event);
    }

    public void sendEvent(CoopDoorStatusMessage.Event event);
    public void sendEvent(CoopDoorStatusMessage.EventCategory eventCategory, String message);
    public List<CoopDoorStatusMessage.Event> getEvents();
    public void clear();


    public void addEventListener(final EventListener eventListener);
    public void removeEventListener(final EventListener eventListener);
}
