package com.worthlessapps.chickencontroller.notifications;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.PinState;
import com.worthlessapps.chickencontroller.config.ChickenCoopConfig;
import com.worthlessapps.chickencontroller.utils.Logger;

import java.util.Arrays;

import static com.worthlessapps.chickencontroller.utils.GPIOUtils.resolvePin;

public class NotificationManagerImpl implements NotificationManager
{

    public Thread _blinkerThread;

    private class LEDState
    {
        public GpioPinDigitalOutput _gpioPin;

        public LEDState(LEDType ledType)
        {
            _ledType = ledType;
        }

        private LEDType _ledType;
        private boolean _blinking;

        private long _nextTransitionAtMs;
        private int _offForMs;
        private boolean _on;
        private int _onForMs;
        private int _numOn;
        private int _targetNumOn;
    }

    private LEDState[] _ledStates = new LEDState[]{new LEDState(LEDType.RED), new LEDState(LEDType.YELLOW), new LEDState(LEDType.GREEN)};

    private final ChickenCoopConfig.HardwareConfiguration.LEDConfiguration _ledConfiguration;
    private boolean _stillRunning = true;

    public NotificationManagerImpl(ChickenCoopConfig.HardwareConfiguration.LEDConfiguration ledConfiguration)
    {
        _ledConfiguration = ledConfiguration;
    }

    public void shutdown()
    {
        setLEDState(LEDType.GREEN, false);
        setLEDState(LEDType.YELLOW, false);
        setLEDState(LEDType.RED, false);
    }

    @Override
    public void blinkLED(LEDType ledType, int numOn, int onForMs, int offForMs)
    {
        if (onForMs < 100 || onForMs > 5000 || offForMs < 100 || offForMs > 5000)
        {
            throw new IllegalArgumentException("Blink rates are not valid: " + onForMs + " : " + offForMs);
        }

        // The GET here will always succeed
        //noinspection OptionalGetWithoutIsPresent
        final LEDState ledState = Arrays.stream(_ledStates).filter(x -> ledType == x._ledType).findFirst().get();

        // Already blinking!
        if (ledState._blinking)
        {
            return;
        }

        ledState._blinking = true;

        ledState._onForMs = onForMs;
        ledState._offForMs = offForMs;
        ledState._numOn = 0;
        ledState._on = true;
        ledState._targetNumOn = numOn;
        ledState._nextTransitionAtMs = System.currentTimeMillis() + ledState._onForMs;
        setLEDState(ledState._ledType, true);
        _blinkerThread.interrupt();

    }

    @Override
    public void stopBlinkLED(LEDType ledType)
    {
        // The GET here will always succeed
        //noinspection OptionalGetWithoutIsPresent
        final LEDState ledState = Arrays.stream(_ledStates).filter(x -> ledType == x._ledType).findFirst().get();

        ledState._blinking = false;
        ledState._on = false;
        ledState._onForMs = 0;
        ledState._offForMs = 0;
        ledState._numOn = 0;
        ledState._targetNumOn = 0;
        _blinkerThread.interrupt();

    }


    @Override
    public void setLEDState(LEDType ledType, boolean stateOn)
    {
        if (isBlinking(ledType))
        {
            return;
        }

        //noinspection OptionalGetWithoutIsPresent
        final LEDState ledState = Arrays.stream(_ledStates).filter(x -> ledType == x._ledType).findFirst().get();

        if (stateOn)
        {
            ledState._gpioPin.high();
        }
        else
        {
            ledState._gpioPin.low();
        }
    }

    private int resolveGPIOPin(LEDType ledType, ChickenCoopConfig.HardwareConfiguration.LEDConfiguration ledConfiguration)
    {
        switch (ledType)
        {
            case GREEN:
                return ledConfiguration._greenLEDGPIO;
            case YELLOW:
                return ledConfiguration._yellowLEDGPIO;
            case RED:
                return ledConfiguration._redLEDGPIO;
            default:
                throw new IllegalStateException("Unhandled LED type: " + ledType);
        }
    }

    private boolean isBlinking(LEDType ledType)
    {
        // The GET here will always succeed
        //noinspection OptionalGetWithoutIsPresent
        final LEDState ledState = Arrays.stream(_ledStates).filter(x -> ledType == x._ledType).findFirst().get();
        return ledState._blinking;
    }

    public void initialize()
    {
        Arrays.stream(_ledStates).forEach((ledState) -> {
            final int gpioPin = resolveGPIOPin(ledState._ledType,_ledConfiguration);
            final GpioController gpio = GpioFactory.getInstance();
            ledState._gpioPin = gpio.provisionDigitalOutputPin(resolvePin(gpioPin), PinState.LOW);
            ledState._gpioPin.setShutdownOptions(true, PinState.LOW);
        });

        _blinkerThread = new Thread(() ->
        {

            while (_stillRunning)
            {
                try
                {
                    // By default we sleep a long time
                    long sleepTimeMs = 5000;
                    if (isBlinkNeeded())
                    {
                        Logger.trace("Blinking is required, will evaluate");

                        for (int i = 0; i < _ledStates.length; i++)
                        {
                            LEDState ledState = _ledStates[i];
                            if (ledState._blinking)
                            {
                                Logger.trace(ledState._ledType + " is blinking, evaluating state");
                                final long currentTimeMs = System.currentTimeMillis();
                                if (ledState._nextTransitionAtMs < currentTimeMs)
                                {
                                    Logger.trace(ledState._ledType + " is blinking and requires toggle");
                                    if (ledState._targetNumOn > 0 && ledState._numOn == ledState._targetNumOn)
                                    {
                                        Logger.trace(ledState._ledType + " has reached target number of blinks, will stop it");
                                        stopBlinkLED(ledState._ledType);
                                        continue;
                                    }

                                    // Action time!
                                    setLEDState(ledState._ledType, !ledState._on);
                                    final long nextTransitionInMs = ledState._on ? ledState._offForMs : ledState._onForMs;
                                    ledState._nextTransitionAtMs = currentTimeMs + nextTransitionInMs;

                                    sleepTimeMs = Math.min(sleepTimeMs, nextTransitionInMs);

                                    if (!ledState._on)
                                    {
                                        ledState._numOn++;
                                    }

                                    ledState._on = !ledState._on;

                                }
                                else
                                {
                                    sleepTimeMs = ledState._nextTransitionAtMs - currentTimeMs;
                                }
                                Logger.trace(ledState._ledType + " should transition in " + sleepTimeMs + " millisecond(s)");
                            }
                        }
                    }
                    final long sanitizedSleepTimeMs = Math.max(100, sleepTimeMs);
                    Logger.trace("Evaluated all LEDs, sleeping for " + sanitizedSleepTimeMs);
                    Thread.sleep(sanitizedSleepTimeMs);
                }
                catch (InterruptedException e)
                {
                    // We have been interrupted, time to loop around
                }
            }


        });
        _blinkerThread.start();
    }

    private boolean isBlinkNeeded()
    {
        for (int i = 0; i < _ledStates.length; i++)
        {
            if (_ledStates[i]._blinking)
            {
                return true;
            }
        }
        return false;
    }

}