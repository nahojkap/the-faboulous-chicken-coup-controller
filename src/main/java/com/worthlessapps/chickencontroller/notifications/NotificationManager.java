package com.worthlessapps.chickencontroller.notifications;

public interface NotificationManager
{
    public enum LEDType
    {
        GREEN, YELLOW, RED
    }
    
    public void blinkLED(LEDType ledType, int numOn, int onForMs, int offForMs);
    public void stopBlinkLED(LEDType ledType);

    public void setLEDState(LEDType ledType, boolean stateOn);
    public void shutdown();
}
