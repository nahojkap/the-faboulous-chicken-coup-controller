package com.worthlessapps.chickencontroller.location;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.worthlessapps.chickencontroller.config.ChickenCoopConfig;
import com.worthlessapps.chickencontroller.utils.Logger;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * @author
 */
public class IPStackLocationResolver extends LocationResolverImpl implements LocationResolver {

    // [0] = IP address
    // [1] = API key
    private static final String URL_FORMAT = "http://api.ipstack.com/%s?access_key=%s&fields=latitude,longitude";

    private final String _apiKey;

    private Location _cachedLocation;

    private static class IPStackResponse {
        @SerializedName("ip")
        String _ip;

        @SerializedName("hostname")
        String _hostname;

        @SerializedName("latitude")
        double _latitude;

        @SerializedName("longitude")
        double _longitude;
    }

/*

{
  "ip": "134.201.250.155",
  "hostname": "134.201.250.155",
  "type": "ipv4",
  "continent_code": "NA",
  "continent_name": "North America",
  "country_code": "US",
  "country_name": "United States",
  "region_code": "CA",
  "region_name": "California",
  "city": "Los Angeles",
  "zip": "90013",
  "latitude": 34.0453,
  "longitude": -118.2413,
  "location": {
    "geoname_id": 5368361,
    "capital": "Washington D.C.",
    "languages": [
        {
          "code": "en",
          "name": "English",
          "native": "English"
        }
    ],
    "country_flag": "https://assets.ipstack.com/images/assets/flags_svg/us.svg",
    "country_flag_emoji": "🇺🇸",
    "country_flag_emoji_unicode": "U+1F1FA U+1F1F8",
    "calling_code": "1",
    "is_eu": false
  },
  "time_zone": {
    "id": "America/Los_Angeles",
    "current_time": "2018-03-29T07:35:08-07:00",
    "gmt_offset": -25200,
    "code": "PDT",
    "is_daylight_saving": true
  },
  "currency": {
    "code": "USD",
    "name": "US Dollar",
    "plural": "US dollars",
    "symbol": "$",
    "symbol_native": "$"
  },
  "connection": {
    "asn": 25876,
    "isp": "Los Angeles Department of Water & Power"
  },
  "security": {
    "is_proxy": false,
    "proxy_type": null,
    "is_crawler": false,
    "crawler_name": null,
    "crawler_type": null,
    "is_tor": false,
    "threat_level": "low",
    "threat_types": null
  }
}



 */


    public IPStackLocationResolver(ChickenCoopConfig chickenCoopConfig) {
        super(chickenCoopConfig);

        if (chickenCoopConfig._serverConfiguration._ipStackConfiguration == null) {
            throw new IllegalStateException("No IpStack configuration");
        }
        _apiKey = chickenCoopConfig._serverConfiguration._ipStackConfiguration._apiKey;

    }

    @Override
    public Future<Location> resolveLocation() {
        return (Future<Location>) Executors.newSingleThreadExecutor().submit(() -> {

            if (_cachedLocation != null)
            {
                Logger.trace("Found cached location, will return this");
                return _cachedLocation;
            }

            try {
                Location location = null;

                final HttpURLConnection ipAddressConnection = (HttpURLConnection) new URL("https://checkip.amazonaws.com/").openConnection();
                ipAddressConnection.connect();

                try {
                    int responseCode = ipAddressConnection.getResponseCode();
                    if (responseCode == 200) {
                        String externalIpAddress;
                        try (final BufferedReader reader = new BufferedReader(new InputStreamReader(ipAddressConnection.getInputStream()))) {
                            externalIpAddress = reader.readLine();
                        }
                        Logger.info("Resolved external IP: " + externalIpAddress);

                        final String ipStackURL = String.format(URL_FORMAT, externalIpAddress, _apiKey);

                        final HttpURLConnection ipStackConnection = (HttpURLConnection) new URL(ipStackURL).openConnection();
                        ipStackConnection.connect();
                        try {

                            responseCode = ipStackConnection.getResponseCode();
                            if (responseCode == 200) {

                                Gson gson = new Gson();

                                try (final BufferedReader reader = new BufferedReader(new InputStreamReader(ipStackConnection.getInputStream()))) {
                                    final IPStackResponse ipStackResponse = gson.fromJson(reader, IPStackResponse.class);

                                    Logger.debug("Parsed IPStack response: " + ipStackResponse._latitude + ":" + ipStackResponse._longitude);
                                    _cachedLocation = new Location(ipStackResponse._latitude, ipStackResponse._longitude);
                                    return _cachedLocation;
                                }
                            }

                        } finally {
                            ipStackConnection.disconnect();
                        }
                    }
                    else {
                        Logger.error("Received non-200 OK while resolving my IP");
                        return super.resolveLocation().get();
                    }
                } finally {
                    ipAddressConnection.disconnect();
                }

                return location;

            } catch (Throwable e) {
                Logger.error("Error resolving IP address: " + e.getMessage(), e);
                return super.resolveLocation().get();
            }
        });
    }

}
