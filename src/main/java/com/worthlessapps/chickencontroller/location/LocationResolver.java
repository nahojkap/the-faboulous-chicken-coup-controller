package com.worthlessapps.chickencontroller.location;

import java.util.concurrent.Future;

/**
 * @author
 */
public interface LocationResolver {

    public class Location
    {
        public double _longitude;
        public double _latitude;

        public Location(double latitude, double longitude) {
            _latitude = latitude;
            _longitude = longitude;
        }
    }

    public Future<Location> resolveLocation();

}
