package com.worthlessapps.chickencontroller.location;

import com.worthlessapps.chickencontroller.config.ChickenCoopConfig;

import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * @author
 */
public class LocationResolverImpl extends AbstractLocationResolverImpl {

    public LocationResolverImpl(ChickenCoopConfig chickenCoopConfig) {
        super(chickenCoopConfig);
    }

    @Override
    public Future<Location> resolveLocation() {

        return (Future<Location>) Executors.newSingleThreadExecutor().submit(() -> {
            return new Location(_chickenCoopConfig._serverConfiguration._location._latitude,_chickenCoopConfig._serverConfiguration._location._longitude);
        });
    }

}
