package com.worthlessapps.chickencontroller.location;

import com.worthlessapps.chickencontroller.config.ChickenCoopConfig;

/**
 * @author
 */
public abstract class AbstractLocationResolverImpl implements LocationResolver {
    
    protected final ChickenCoopConfig _chickenCoopConfig;

    public AbstractLocationResolverImpl(ChickenCoopConfig chickenCoopConfig) {
        _chickenCoopConfig = chickenCoopConfig;
    }

    public void initialize() {
    }
}
