package com.worthlessapps.chickencontroller;

public interface CoopDoor
{
    public enum CoupDoorState {
        UNKNOWN, OPENING, OPEN, CLOSING, CLOSED, ERROR
    }

    CoupDoorState getCoupDoorState();

    void open(boolean unconditional);

    void down(int durationMs);

    void up(int durationMs);

    void close(boolean unconditional);
}
