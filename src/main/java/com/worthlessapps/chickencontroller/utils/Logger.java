package com.worthlessapps.chickencontroller.utils;

public class Logger {

    final static String TRACE_FORMAT_STRING =   "[T][%s] %s";
    final static String DEBUG_FORMAT_STRING =   "[D][%s] %s";
    final static String INFO_FORMAT_STRING =    "[I][%s] %s";
    final static String WARN_FORMAT_STRING =    "[W][%s] %s";
    final static String ERROR_FORMAT_STRING =   "[E][%s] %s";

    public enum LogLevel
    {
        TRACE, DEBUG, INFO, WARN, ERROR
    }

    private static final String TAG = "ChickenCoup";
    private static LogLevel CURRENT_LOG_LEVEL = LogLevel.DEBUG;

    public static void setLogLevel(LogLevel logLevel)
    {
        CURRENT_LOG_LEVEL = logLevel;
    }

    public static void trace(String message)
    {
        if (CURRENT_LOG_LEVEL.ordinal() <= LogLevel.TRACE.ordinal()) {
            System.out.println(String.format(TRACE_FORMAT_STRING, TAG, message));
        }
    }

    public static void debug(String message)
    {
        if (CURRENT_LOG_LEVEL.ordinal() <= LogLevel.DEBUG.ordinal()) {
            System.out.println(String.format(DEBUG_FORMAT_STRING, TAG, message));
        }
    }

    public static void info(String message)
    {
        if (CURRENT_LOG_LEVEL.ordinal() <= LogLevel.INFO.ordinal()) {
            System.out.println(String.format(INFO_FORMAT_STRING, TAG, message));
        }
    }

    public static void warn(String message)
    {
        if (CURRENT_LOG_LEVEL.ordinal() <= LogLevel.WARN.ordinal()) {
            System.out.println(String.format(WARN_FORMAT_STRING, TAG, message));
        }
    }

    public static void error(String message)
    {
        if (CURRENT_LOG_LEVEL.ordinal() <= LogLevel.ERROR.ordinal()) {
            System.out.println(String.format(ERROR_FORMAT_STRING, TAG, message));
        }
    }

    public static void error(String message, Throwable t)
    {
        if (CURRENT_LOG_LEVEL.ordinal() <= LogLevel.ERROR.ordinal()) {
            System.out.println(String.format(ERROR_FORMAT_STRING, TAG, message));
            t.printStackTrace();
        }
    }
}
