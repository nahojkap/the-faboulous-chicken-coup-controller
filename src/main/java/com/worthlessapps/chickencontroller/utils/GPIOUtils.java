package com.worthlessapps.chickencontroller.utils;

import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.RaspiPin;

public class GPIOUtils {
    public static Pin resolvePin(int pin) {
        return RaspiPin.getPinByAddress(pin);
    }

}
