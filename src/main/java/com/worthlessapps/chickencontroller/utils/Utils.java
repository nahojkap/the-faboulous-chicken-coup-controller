package com.worthlessapps.chickencontroller.utils;

public class Utils {

    public static void exceptionLessSleep(int sleepTimeMs)
    {
        try
        {
            Thread.sleep(sleepTimeMs);
        }
        catch (InterruptedException e)
        {
            // Nothing to do here
        }
    }
}
