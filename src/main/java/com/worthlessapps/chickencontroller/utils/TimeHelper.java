package com.worthlessapps.chickencontroller.utils;

import org.joda.time.DateTime;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author
 */
public class TimeHelper {

    /** Matches a time of the day pattern - 07:00 or 7:00am or 23:59 or 11:59pm etc
     */
    private static final Pattern SIMPLE_TIME_PATTERN = Pattern.compile("^(([0-1]?[0-9]|2[0-3]):([0-5][0-9]))|(([1]?[0-2]|[1-9]):([0-5][0-9])\\s*([ap]m))$", Pattern.CASE_INSENSITIVE);
    private static final Pattern SUNRISE_SUNSET_PATTERN = Pattern.compile("(sunset|sunrise)\\s*([+\\-])\\s*(\\d{1,2})\\s*(m*)", Pattern.CASE_INSENSITIVE);
    private static final Pattern DURATION_PATTERN = Pattern.compile("(\\d{1,3})([mhd])*",Pattern.CASE_INSENSITIVE);

    public static Matcher createSunriseSunsetMatcher(String input)
    {
        return SUNRISE_SUNSET_PATTERN.matcher(input);
    }

    public static Matcher createTimeMatcher(String input)
    {
        return SIMPLE_TIME_PATTERN.matcher(input);
    }

    public static Matcher createDurationMatcher(String input)
    {
        return DURATION_PATTERN.matcher(input);
    }

    public static DateTime getMidnight(long currentTimeMs)
    {
        return new DateTime(currentTimeMs).withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0).withMillisOfDay(0);
    }

    public static DateTime getMidnight()
    {
        return getMidnight(System.currentTimeMillis());
    }

    public static long addStringifiedTime(long currentTimeMs, String stringifiedTime)
    {
        final Matcher matcher = SIMPLE_TIME_PATTERN.matcher(stringifiedTime);
        if (matcher.matches())
        {
            int hours;
            int minutes;

            // We have a time!!
            if (matcher.group(7) != null) {
                // 12h time!
                hours = Integer.parseInt(matcher.group(5));
                minutes = Integer.parseInt(matcher.group(6));
                if (matcher.group(7).equalsIgnoreCase("pm"))
                {
                    hours += 12;
                }
            }
            else {
                // 24h time!
                hours = Integer.parseInt(matcher.group(2));
                minutes = Integer.parseInt(matcher.group(3));
            }

            // Create date from today @ midnight
            DateTime dateTime = new DateTime(currentTimeMs).withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0).withMillisOfDay(0);

            // Add the hours & minutes to it, return the new date in MS
            return dateTime.plusHours(hours).plusMinutes(minutes).getMillis();

        }
        return -1;
    }

    public interface TimeResolver
    {
        DateTime getCurrentTime();
    }

}
